<?php
/*
 * Get all statistics of database framastats_history
 * BEWARE FOR THE PATH : include by index.php
 * */
// ################## INCLUDE #####################
$pathToUtil_mySQL 	= '../../../private/dirFramastats/framastats/scripts/mySQL/Util_mySQL.php';
$pathToUtil		= '../../../private/dirFramastats/framastats/scripts/utils/Util.php';
include ($pathToUtil_mySQL);
include ($pathToUtil);

// ############### UTILS & DEBUG ##################
$util = new Util();

// ################# CONSTANTS #################
$pathDbInfos		= '../../../private/dirFramastats/dbInfos_Framastats.json';
$defaultInfo		= 'TO_CHANGE';
$tableFramastats_hstry	= 'framastats_history';
$columnService 		= 'service';
$columnNameStat 	= 'nameStat';
$columnValueStat 	= 'valueStat';
$columnDate 		= 'date';
$tableFramastat_srvc	= 'framastats_service';
$yearCreationFramasoft	= '2001';
$projectInProgress	= '6';
$membersNumber		= '30';
$voluntaryWorkHours	= 'XXXX';
$donations		= 'XXXX';
$recurrentDonations	= 'XXXX';

// #################### DATABASE ##################
$dbInfos = array (
	'db_host' 	=> $defaultInfo,
	'db_dbname' 	=> $defaultInfo,
	'db_usr' 	=> $defaultInfo,
	'db_pswrd' 	=> $defaultInfo
	);

// ############## FUNCTIONS ###################

/* return $stat with a french number format (15670,15 => 15 670,1) */
function frenchNumberFormat ($stat) {
	global $util;

	// repare conversion errors
	$stat = (0+str_replace(",",".",$stat));

        if(is_float($stat)) {
		$stat = number_format($stat, 2, ',', ' ');
	} else {
		$stat = number_format($stat, 0, ',', ' ');
	}
	return $stat;
}


/* display in html @statName thanks to @sql query  in @dBase with result in @columnValueStat */
function displayOneValueInHTML ($dBase, $sql, $columnValueStat, $statName, $isItANumber = false) {
	$res 	= $dBase->query($sql);
	$stat 	= $res->fetch()[$columnValueStat];

	if($isItANumber){
		$stat = frenchNumberFormat ($stat);
	}

	echo PHP_EOL . "<span class='" . $statName . "'>" . $stat . "</span>";
}

/* Retrieve one @statName of @serviceName in @dBase */
function handleQuery_oneValue ($dBase, $statName, $serviceName, $isItANumber = false) {

	global $tableFramastats_hstry;
	global $columnService;
	global $columnNameStat;
	global $columnValueStat;
	global $columnDate;

	$sql = "SELECT $columnValueStat, $columnDate
		FROM $tableFramastats_hstry
		WHERE $columnService LIKE '%$serviceName%' AND $columnNameStat LIKE '%" . $statName . "%'
		AND $columnDate NOT LIKE '%00:0%'
		ORDER BY $columnDate DESC
		LIMIT 1";

	$finalStatName = $serviceName . '_'. $statName;
	displayOneValueInHTML ($dBase, $sql, $columnValueStat, $finalStatName, $isItANumber);
}

/* Calculate and display the average with no time limit for one stat (@statName) of @serviceName in @dBase */
function handleQuery_oneAverageValue ($dBase, $statName, $serviceName, $isItANumber = false) {

	global $tableFramastats_hstry;
	global $columnService;
	global $columnNameStat;
	global $columnValueStat;
	global $columnDate;

	$sql = "SELECT AVG(max) as avg FROM (
			SELECT MAX(CAST($columnValueStat AS UNSIGNED)) as max, DATE(date) as day
				FROM `$tableFramastats_hstry`
				WHERE $columnService LIKE '%$serviceName%'
				AND $columnNameStat LIKE '%$statName%'
				AND $columnDate NOT LIKE '%00:0%'
				AND DATE($columnDate) NOT LIKE CURDATE()
			GROUP BY day ) as maxPerDay";

	$finalStatName = $serviceName . '_'. $statName . '_AVG';
	displayOneValueInHTML ($dBase, $sql, 'avg', $finalStatName, $isItANumber);
}

/* call by handleQuery_topValue */
function displaytopValueInHTML ($dBase, $sql, $columnTopValueStat = 'top', $statName, $isItANumber = false) {
	global $columnDate;

	$res 		= $dBase->query($sql);
	while ($data = $res->fetch()) {
		$topStat 	= $data[$columnTopValueStat];
		$dateTime	= $data[$columnDate];
	}
	$res->closeCursor();

	$time 		= strtotime($dateTime);
	$frenchDateTime	= date("d/m/y", $time);


	if($isItANumber){
		$topStat = frenchNumberFormat ($topStat);
	}

	echo PHP_EOL . "<span class='" . $statName . "_date'>" . $frenchDateTime . "</span>";
	echo PHP_EOL . "<span class='" . $statName . "_value'>" . $topStat . "</span>";
}

/* Retrieve the best @statName of @serviceName in @dBase */
function handleQuery_topValue ($dBase, $statName, $serviceName, $isItANumber = false) {

	global $tableFramastats_hstry;
	global $columnService;
	global $columnNameStat;
	global $columnDate;
	global $columnValueStat;

	$sql = "SELECT CAST($columnValueStat AS UNSIGNED) as top, $columnDate
		FROM $tableFramastats_hstry
		WHERE $columnService LIKE '%$serviceName%' AND $columnNameStat LIKE '%" . $statName . "%'
		AND $columnDate NOT LIKE '%00:0%'
		ORDER BY top desc
		LIMIT 1";

	$topStatName = $serviceName . '_TOP_' . $statName;
	$columnTopValueStat = 'top';

	displaytopValueInHTML ($dBase, $sql, $columnTopValueStat, $topStatName, $isItANumber);
}

/* Retrieve and display the rank of service according to @statName */
function handleQuery_RankingService ($dBase, $statName) {

	global $tableFramastats_hstry;
	global $columnService;
	global $columnNameStat;
	global $columnDate;
	global $columnValueStat;

	$val = 'val';

	$sql = "SELECT CAST($columnValueStat AS UNSIGNED) as $val, $columnService
		FROM (
			SELECT *
			FROM $tableFramastats_hstry
			ORDER BY $columnDate desc
		) as lastRecord
		WHERE $columnNameStat LIKE '%" . $statName . "%'
		AND $columnDate NOT LIKE '%00:0%'
		GROUP BY $columnService
		ORDER BY $val desc";

	// Execute query
	$res 			= $dBase->query($sql);
	// Look through rank
	$it = 1;
	while ($data = $res->fetch()) {
		$service 	= $data[$columnService];
		if ($service == 'framasphereStat') { $service = 'Framasphere'; }
		elseif ($service == 'framalibre') { $service = 'Framasoft'; }
		$service 	= ucfirst($service); 
		$value		= frenchNumberFormat ($data[$val]);
		$nameClass	= 'rank_' . $statName . '_' . (string)$it;
		echo PHP_EOL . "<span class='" . $nameClass . "_service'>" . $service . "</span>";
		echo PHP_EOL . "<span class='" . $nameClass . "_value'>" . $value . "</span>";
		$it += 1;
	}
	$res->closeCursor();


}

/* Retrieve and display the top of stats
 *
 * Stats should be like :
 *
 *            nameStat      | value
 * =====================================
 * patternNameTop_0_Alice   | 44
 * patternNameTop_1_Bob     | 28
 * patternNameTop_2_Quentin | 21
 * ...                      | ...
 *
 * And we want to retrieve just :
 *
 * Alice                    | 44
 * Bob                      | 28
 * Quentin                  | 21
 * ...                      | ...
 *
 * To create :
 *
 * <span class='@service_@classStatName_rank_0_name'>Alice</span>
 * <span class='@service_@classStatName_rank_0_value'>44</span>
 * <span class='@service_@classStatName_rank_1_name'>Bob</span>
 * <span class='@service_@classStatName_rank_1_value'>28</span>
 * ...
 *
 *  */
function handleQuery_RankTop ($dBase, $service, $patternNameTop, $arrayForbiddenStat, $classStatName, $isItANumber = false) {

	global $tableFramastats_hstry;
	global $columnService;
	global $columnNameStat;
	global $columnDate;
	global $columnValueStat;
	global $util;
	$top = 'top';
	$regex = "/[0-9]_(.*)/"; // capturing group 1 ==> real stat name

	// Nested query : retrieve last update date for this stat
	$sql = "SELECT CAST(valueStat AS UNSIGNED) as $top, $columnNameStat, $columnDate
		FROM $tableFramastats_hstry
		WHERE $columnDate = 	(SELECT $columnDate
					FROM $tableFramastats_hstry
					WHERE $columnService LIKE '%" . $service . "%' AND nameStat LIKE '" . $patternNameTop . "%'
					AND $columnDate NOT LIKE '%00:0%'
					ORDER BY $columnDate desc LIMIT 1
					)
		AND $columnNameStat LIKE '" . $patternNameTop . "%'
		ORDER BY $top desc";

	$res = $dBase->query($sql);
	// Look through rank
	$it = 0;
	while ($data = $res->fetch()) {
		// Retrieve datas
		$nameStat 	= $data[$columnNameStat];
		$value		= $data[$top];

		if ($isItANumber) {
			$value = frenchNumberFormat ($value);
		}

		// Retrieve real name thks to regex
		if (1 == preg_match($regex, $nameStat, $matches)) { // pattern matches
			if (!in_array($matches[1], $arrayForbiddenStat)) { // if not in forbidden array
				$realNameStat 	= $matches[1];
				$className	= $service . '_'. $classStatName . '_rank_' . (string)$it;
				echo PHP_EOL . "<span class='" . $className . "_name'>" . $realNameStat . "</span>";
				echo PHP_EOL . "<span class='" . $className . "_value'>" . $value . "</span>";
				$it += 1;
			} else {
				continue;
			}
		} else {
			$util->out("### handleQuery_RankTop : pattern doesn't matches to find stat name for $nameStat.", 'error', true);
		}
	}
	$res->closeCursor();
}

/* call by handleQuery_lastUpdate */
function displayUpdateInHTML ($dBase, $sql, $serviceName) {
	global $columnDate;

	$res		= $dBase->query($sql);
	$dateTime 	= $res->fetch()[$columnDate];
	$res->closeCursor();

	$time 		= strtotime($dateTime);
	$dayOfUpdate	= date("d/m/y");

	$today 		= date("d/m/y");
	if ($dayOfUpdate == $today) {
		$timeUpdate = date("G\hi", $time);
	} else {
		$timeUpdate = date("d/m/y", $time);
	}

	echo PHP_EOL . "<span class='" . $serviceName . "_lastUpdate'>" . $timeUpdate . "</span>";
}

/*
 * Display time of update if it's today
 * or display day of update
 * @param $serviceName : only need service Name
 *
 * */
function handleQuery_lastUpdate ($dBase, $serviceName) {

	global $tableFramastats_hstry;
	global $columnService;
	global $columnDate;

	$sql = "SELECT $columnDate
		FROM $tableFramastats_hstry
		WHERE $columnService LIKE '%$serviceName%'
		ORDER BY $columnDate desc
		LIMIT 1";

	displayUpdateInHTML ($dBase, $sql, $serviceName);
}

// ########### DATABASE CONNECTION ###########

// Manage database informations
$finalDbInfos = $util->checkVariablesOrSetThem($pathDbInfos, $dbInfos, $defaultInfo);

// Database connection
try
{
	$dBase = new PDO('mysql:host=' . $finalDbInfos['db_host'] . ';dbname='. $finalDbInfos['db_dbname'] . ';charset=utf8', $finalDbInfos['db_usr'], $finalDbInfos['db_pswrd']);
	$util->out("### Established connection in the database : " . $finalDbInfos['db_dbname'], "success");
}
catch(Exception $e)
{
	$util->out("### Error in database connection : ".$e->getMessage(), "error");
	$util->out("### Check infos in this file : ". $pathDbInfos, "error", true);
}

// ############################################
// ############################################
// ############## FRAMASOFT ###################
$tmp_serviceName = 'framastats';

/* Years of the network Framasoft and the organization */
$start 				= date($yearCreationFramasoft);
$end  				= date('Y');
$stat_yearsFramasoft 		= $end - $start ;
$stat_yearsFramasoft_org	= $stat_yearsFramasoft - 3 ;

echo "<span class='framastats_years'>" . $stat_yearsFramasoft . "</span>";
echo PHP_EOL . "<span class='framastats_years_organization'>" . $stat_yearsFramasoft_org . "</span>";

/* Embedded Services */
$sql = "SELECT COUNT(DISTINCT $columnService) as count
	FROM `$tableFramastats_hstry`";
displayOneValueInHTML ($dBase, $sql, 'count', 'framasoft_countEmbeddedServices', true);

/* Projects in Progress */
echo PHP_EOL . "<span class='framastats_projectInProgress'>" . $projectInProgress . "</span>";

/* Members */
echo PHP_EOL . "<span class='framastats_members'>" . $membersNumber . "</span>";

/* Voluntary work hours*/
echo PHP_EOL . "<span class='framastats_voluntaryWorkHours'>" . $voluntaryWorkHours . "</span>";

/* Donations */
echo PHP_EOL . "<span class='framastats_donations'>" . $donations . "</span>";

/* Recurrent donations */
echo PHP_EOL . "<span class='framastats_recurrentDonations'>" . $recurrentDonations . "</span>";

/* Visits of all services */
$framastats_arrayStat = array('rest_json_nbVisits_all_total',
			'rest_json_nbVisits_thisYear_total',
			'rest_json_nbVisits_last365days_total',
			'rest_json_nbVisits_last180days_total',
			'rest_json_nbVisits_last90days_total',
			'rest_json_nbVisits_last30days_total',
			'rest_json_nbVisits_last7days_total',
			'rest_json_nbVisits_yesterday_total',
			'rest_json_nbVisits_today_total');
foreach ($framastats_arrayStat as $nameStat){
	handleQuery_oneValue ($dBase, $nameStat, $tmp_serviceName, true);
}

/* Day with more visits */
handleQuery_topValue ($dBase, 'rest_json_nbVisits_today_total', $tmp_serviceName, true);

/* Average visits */
handleQuery_oneAverageValue ($dBase, 'rest_json_nbVisits_today_total', $tmp_serviceName, true);

/* Last Update */
handleQuery_lastUpdate ($dBase, $tmp_serviceName);

// ############## FRAMADATE ###################
$tmp_serviceName = 'framadate';
$tmp_arrayStat = array('mySQL_nbPulls_all_living',
			'mySQL_nbPulls_1lastYear_living',
			'mySQL_nbPulls_6lastMonths_living',
			'mySQL_nbPulls_3lastMonths_created',
			'mySQL_nbPulls_1lastMonth_created',
			'mySQL_nbPulls_1lastWeek_created',
			'mySQL_nbPulls_today_created',
			'mySQL_pulls_lifeExpectancyInDays',
			'mySQL_pulls_avgNbUsers',
			'mySQL_pulls_formats_0_',
			'mySQL_pulls_formats_1_',
			'mySQL_pulls_formats_2_',
			'mySQL_pulls_formats_4_',
			'mySQL_pulls_formats_3_');
foreach ($tmp_arrayStat as $nameStat){
	handleQuery_oneValue ($dBase, $nameStat, $tmp_serviceName, true);
}
handleQuery_topValue ($dBase, 'mySQL_nbPulls_today_created', $tmp_serviceName, true);
handleQuery_oneAverageValue ($dBase, 'mySQL_nbPulls_today_created', $tmp_serviceName, true);
handleQuery_lastUpdate ($dBase, $tmp_serviceName);

// ############## FRAMAPAD ###################
$tmp_serviceName = 'framapad';
$tmp_arrayStat = array('rest_json_totalPads_living',
			'rest_json_totalBlankPads_living',
			'rest_json_totalBlankPads_living_percent',
			'rest_json_pluginFramapad_quotidien_padsCount',
			'rest_json_pluginFramapad_hebdo_padsCount',
			'rest_json_pluginFramapad_mensuel_padsCount',
			'rest_json_pluginFramapad_bimestriel_padsCount',
			'rest_json_pluginFramapad_semestriel_padsCount',
			'rest_json_pluginFramapad_annuel_padsCount',
			'rest_json_pluginFramapad_eternal_padsCount');
foreach ($tmp_arrayStat as $nameStat){
	handleQuery_oneValue ($dBase, $nameStat, $tmp_serviceName, true);
}
handleQuery_lastUpdate ($dBase, $tmp_serviceName);

// ############## FRAMALIBRE ################
$tmp_serviceName = 'framalibre';
$tmp_arrayStat = array('mySQL_nbNotices_all',
			'mySQL_nbNotices_1lastYear',
			'mySQL_nbNotices_6lastMonths',
			'mySQL_nbNotices_3lastMonths',
			'mySQL_nbArticlesTribune_all',
			'mySQL_nbTutoriels_all',
			'mySQL_nbAuthors',
			'mySQL_nbArticlesWithSeveralAuthors',
			'mySQL_nbComments_all',
			'mySQL_nbComments_1lastYear',
			'mySQL_nbComments_6lastMonths',
			'mySQL_nbComments_3lastMonths',
			'mySQL_nbComments_1lastMonth',
			'mySQL_nbComments_1lastWeek',
			'mySQL_nbComments_today');
foreach ($tmp_arrayStat as $nameStat){
	handleQuery_oneValue ($dBase, $nameStat, $tmp_serviceName, true);
}
handleQuery_lastUpdate ($dBase, $tmp_serviceName);

// ############## FRAMASPHERE #############
$tmp_serviceName = 'framasphere';
$tmp_arrayStat = array('total_users',
			'active_users_monthly',
			'active_users_halfyear',
			'local_posts',
			'local_comments');
foreach ($tmp_arrayStat as $nameStat){
	handleQuery_oneValue ($dBase, $nameStat, $tmp_serviceName, true);
}
handleQuery_lastUpdate ($dBase, $tmp_serviceName);

// ############## FRAMABLOG ################
$tmp_serviceName = 'framablog';
$patternStatCategory = 'mySQL_topCategory';
$forbiddenStat_blog = array('Planet','Traduction','RezoTIC','Librevolution');

$tmp_arrayStat = array('mySQL_nbPosts_all',
			'mySQL_nbPosts_1lastYear',
			'mySQL_nbPosts_6lastMonths',
			'mySQL_nbPosts_3lastMonths',
			'mySQL_nbPosts_1lastMonth',
			'mySQL_nbPosts_1lastWeek',
			'mySQL_nbPosts_today',
			'mySQL_nbCategory',
			'mySQL_nbComments_all',
			'mySQL_nbComments_1lastYear',
			'mySQL_nbComments_6lastMonths',
			'mySQL_nbComments_3lastMonths',
			'mySQL_nbComments_1lastMonth',
			'mySQL_nbComments_1lastWeek',
			'mySQL_nbComments_today',
			'mySQL_nbComments_1lastWeek',
			);
foreach ($tmp_arrayStat as $nameStat){
	handleQuery_oneValue ($dBase, $nameStat, $tmp_serviceName, true);
}
handleQuery_RankTop ($dBase, $tmp_serviceName, $patternStatCategory, $forbiddenStat_blog, 'category', true);
handleQuery_lastUpdate ($dBase, $tmp_serviceName);

// ############## FRAMABOOK #################
$tmp_serviceName = 'framabook';
$patternStatDownloads = 'mySQL_topDownloads';
$patternStatDownloadsWeek = 'mySQL_topDownloadsThisWeek';
$forbiddenStat_blog = array();

$tmp_arrayStat = array('mySQL_nbBooks',
			'mySQL_nbDwnlds_all',
			'mySQL_nbDwnlds_1lastYear',
			'mySQL_nbDwnlds_6lastMonths',
			'mySQL_nbDwnlds_3lastMonths',
			'mySQL_nbDwnlds_1lastMonth',
			'mySQL_nbDwnlds_1lastWeek',
			'mySQL_nbDwnlds_today'
			);
foreach ($tmp_arrayStat as $nameStat){
	handleQuery_oneValue ($dBase, $nameStat, $tmp_serviceName, true);
}
handleQuery_RankTop ($dBase, $tmp_serviceName, $patternStatDownloads, $forbiddenStat_blog, 'downloadsAll', true);
handleQuery_RankTop ($dBase, $tmp_serviceName, $patternStatDownloadsWeek, $forbiddenStat_blog, 'downloadsThisWeek', true);
handleQuery_topValue ($dBase, 'mySQL_nbDwnlds_today', $tmp_serviceName, true);
handleQuery_oneAverageValue ($dBase, 'mySQL_nbDwnlds_today', $tmp_serviceName, true);
handleQuery_lastUpdate ($dBase, $tmp_serviceName);

// ################ FRAMADVD ##################
$tmp_serviceName = 'framadvd';
$tmp_arrayStat = array('logs_downloads_today_ecole',
			'logs_downloads_today_classic',
			'logs_downloads_all');
foreach ($tmp_arrayStat as $nameStat){
	handleQuery_oneValue ($dBase, $nameStat, $tmp_serviceName, true);
}
handleQuery_topValue ($dBase, 'logs_downloads_today_ecole', $tmp_serviceName, true);
handleQuery_topValue ($dBase, 'logs_downloads_today_classic', $tmp_serviceName, true);
handleQuery_lastUpdate ($dBase, $tmp_serviceName);

// ################ FRAMABIN ###################
$tmp_serviceName = 'framabin';
$tmp_arrayStat = array(	'logs_docCreated_today',
			'logs_docCreated_all',
			'logs_sharedDocs_today',
			'logs_sharedDocs_all');
foreach ($tmp_arrayStat as $nameStat){
	handleQuery_oneValue ($dBase, $nameStat, $tmp_serviceName, true);
}
$tmp_arrayStat = array('logs_docCreated_today',
			'logs_sharedDocs_today');
foreach ($tmp_arrayStat as $nameStat){
	handleQuery_topValue ($dBase, $nameStat, $tmp_serviceName, true);
}
handleQuery_lastUpdate ($dBase, $tmp_serviceName);

// ############## SOCIAL NETWORKS #############
$tmp_serviceName = 'reseauxSociaux';
$tmp_arrayStat = array('twitter_nbFollowers',
			'twitter_nbTweets',
			'twitter_nbFriends',
			'facebook_nbLikes');
foreach ($tmp_arrayStat as $nameStat){
	handleQuery_oneValue ($dBase, $nameStat, $tmp_serviceName, true);
}
handleQuery_lastUpdate ($dBase, $tmp_serviceName);

// TODO : not display in index.php
// ################# VISITS ###################
handleQuery_RankingService ($dBase, 'pwk_nbVisits_all');

/*
// ################ PATTERN ###################
$tmp_serviceName = 'framaporn';
$tmp_arrayStat = array('statName1',
			'statName2',
			'statName3',
			'statName4');
foreach ($tmp_arrayStat as $nameStat){
	handleQuery_oneValue ($dBase, $nameStat, $tmp_serviceName, true);
}
handleQuery_lastUpdate ($dBase, $tmp_serviceName);
*/
?>
