<?php
/*
 * Create all historic stats that will be useful for charts in framastats.org
 * Used stats recorded in framastats database
 *
 * Use Chart.js library
 * Doc here : http://www.chartjs.org/docs/
 *
 * */

// ################# CONSTANTS #################
//$pathToDirFrama	= '../private/dirFramastats/';
$pathToDirFrama		= '../../../private/dirFramastats/';

$pathToUtil_mySQL 	= $pathToDirFrama . 'framastats/scripts/mySQL/Util_mySQL.php';
$pathToUtil		= $pathToDirFrama . 'framastats/scripts/utils/Util.php';
$pathDbInfos		= $pathToDirFrama . '/dbInfos_Framastats.json';

$defaultInfo		= 'TO_CHANGE';
$tableFramastats_hstry	= 'framastats_history';
$tableFramastat_srvc	= 'framastats_service';
$columnService 		= 'service';
$columnNameStat 	= 'nameStat';
$columnValueStat 	= 'valueStat';
$columnDate 		= 'date';

$light_blue_background	= "rgba(178, 213, 219, 0.4)";
$light_blue		= "rgba(178, 213, 219, 1)";
$light_grey_background	= "rgba(189, 189, 189, 0.4)";
$light_grey		= "rgba(189, 189, 189, 1)";
$light_violet_background= "rgba(129, 87, 194, 0.4)";
$light_violet		= "rgba(129, 87, 194, 1)";

$datasetOptions_1 	= array ('fillColor' => $light_blue_background, 'strokeColor'=> $light_blue,
				'pointColor'=> $light_blue, 'pointStrokeColor'=> "white",
				'pointHighlightFill'=> "white",'pointHighlightStroke'=> $light_blue);
$datasetOptions_2 	= array ('fillColor' => $light_grey_background, 'strokeColor'=> $light_grey,
				'pointColor'=> $light_grey, 'pointStrokeColor'=> "white",
				'pointHighlightFill'=> "white",'pointHighlightStroke'=> $light_grey);
$datasetOptions_3 	= array ('fillColor' => $light_violet_background, 'strokeColor'=> $light_violet,
				'pointColor'=> $light_violet, 'pointStrokeColor'=> "white",
				'pointHighlightFill'=> "white",'pointHighlightStroke'=> $light_violet);

// *** array of datasetOptions
$arrayOfArrayDatasetsOptions = array ($datasetOptions_1, $datasetOptions_2, $datasetOptions_3);

// ################## INCLUDE #####################
include ($pathToUtil_mySQL);
include ($pathToUtil);

// ############### UTILS & DEBUG ##################
$util = new Util(); // Display only errors as : $util->out("HELLO WORLD", "error");

// #################### DATABASE ##################
$dbInfos = array (
	'db_host' 	=> $defaultInfo,
	'db_dbname' 	=> $defaultInfo,
	'db_usr' 	=> $defaultInfo,
	'db_pswrd' 	=> $defaultInfo
	);

// ########## DATABASE CONNECTION #############
// Manage database informations
$finalDbInfos = $util->checkVariablesOrSetThem($pathDbInfos, $dbInfos, $defaultInfo);

// Database connection
try
{
	$dBase = new PDO('mysql:host=' . $finalDbInfos['db_host'] . ';dbname='. $finalDbInfos['db_dbname'] . ';charset=utf8', $finalDbInfos['db_usr'], $finalDbInfos['db_pswrd']);
	$util->out("### Established connection in the database : " . $finalDbInfos['db_dbname'], "success");
}
catch(Exception $e)
{
	$util->out("### Error in database connection : ".$e->getMessage(), "error");
	$util->out("### Check infos in this file : ". $pathDbInfos, "error", true);
}

// ################# FUNCTIONS ###################

/*
 * Called by function handleChartsDatas_DaysAndValues
 *
 * Transforme arrays of labels (@arrayLabels)
 * and datas (@arrayOfArrayDatas) and options (@arrayOfArrayOptions)
 * to JSON format for Chart.js (see js/script_index_Chart.js)
 * @return JSON format
 *
 * WARNING
 *
 * @arrayOfArrayDatas and @arrayOfArrayOptions should correspond i.e :
 * (arrayOfDatas_Bob,arrayOfDatas_Alice,arrayOfDatas_Aurore) && (arrayOfOptions_Bob,arrayOfOptions_Alice,arrayOfOptions_Aurore)
 *
 * */
function handleChartFormat ($arrayLabels, $arrayOfArrayDatas, $arrayOfArrayOptions = null) {

	global $arrayOfArrayDatasetsOptions;
	$howManyDatasetsOptions = count($arrayOfArrayDatasetsOptions) - 1;
	$datasets = array();

	// labels
	$labels	= array ('labels' => $arrayLabels);

	$i = 0;
	// datasets : add each options
	foreach ($arrayOfArrayDatas as $arrayDatas) {

		// choice of default values
		if ($i == $howManyDatasetsOptions) {
			$choice	= $i;
		} else {
			$choice = ($i % $howManyDatasetsOptions);
		}
		$datasetsOptions = $arrayOfArrayDatasetsOptions[$choice];

		// add datas to default values
		$datasetsOptions['data'] = $arrayDatas;

		// add options for this dataset
		if ($arrayOfArrayOptions != null) {
			$tmp_datasetsOptions = array();

			// many options for this dataset
			foreach ($arrayOfArrayOptions[$i] as $optionName=>$value) {
				$tmp_datasetsOptions[$optionName] = $value;
			}

			// merge options
			$datasetsOptions = array_merge($datasetsOptions, $tmp_datasetsOptions);
		}

		// merge all options in datasets
		$datasets[] = $datasetsOptions;

		// reinitialize for next one
		unset($datasetsOptions['data']);

		$i += 1;

	}

	// all
	$all['labels'] = $arrayLabels;
	$all['datasets'] = $datasets;

	// made this json
	$json = json_encode($all);
	return $json;
}

/*
 * Called by function handleChartsDatas_DaysAndValues
 *
 * Display HTML code for chart stats (@jsonStats)
 * Ready to be deploy with JS
 * */
function displayChartStats ($nameStats, $jsonStats) {

	echo "<span class='chartsStats " . $nameStats . "'>";
	echo $jsonStats;
	echo "</span>" . PHP_EOL;

}

/*
 * Create (Line) Charts (@chartName) with Days in x-axis and Values in y-axis
 * Can have multiple Values thanks to the @arrayOfStatsName but with the same x-axis
 *
 * WARNING : DON'T CHECK IF Days ARE THE SAME FOR ALL Values AND TAKE Days OF THE FIRST Values
 *
 * @param service : framasoft/date...
 * */
function handleChartsDatas_DaysAndValues ($dBase, $service, $chartName, $arrayOfStatsName, $dateFirstStat = '2015-07-24', $arrayOfArrayOptions = null) {

	global $columnDate;
	global $columnValueStat;
	global $columnNameStat;
	global $columnService;
	global $tableFramastats_hstry;

	// Initialize
	$arrayDatasets = array();
	$array_days = array();

	$i = 0;
	// for each stat, one dataset is created
	foreach ($arrayOfStatsName as $statName) {

		// Initialize for each dataset
		$array_values = array();

		// Avoid stats from last day but recorded in current day with "$columnDate NOT LIKE '%00:0%'"
		// Limit the charts to the last 30 results
		$res = $dBase->query("SELECT * FROM (SELECT DATE($columnDate) as day, MAX(CAST($columnValueStat AS UNSIGNED)) as max
						FROM `$tableFramastats_hstry`
						WHERE `$columnService` = '$service'
						AND `$columnNameStat` = '$statName'
						AND $columnDate >= '$dateFirstStat'
						AND $columnDate NOT LIKE '%00:0%'
						GROUP BY day ORDER BY day DESC LIMIT 30) as allResult
					ORDER BY day ASC");

		while ($data = $res->fetch())
		{
			if ($i == 0) {$array_days[] = $data['day'];} // same days for each datasets
			$array_values[] = (int) $data['max'];
		}
		$res->closeCursor();

		// Add datasets
		$arrayDatasets[] = $array_values;

		$i += 1;
	}

	// change data in json format
	$jsonStats = handleChartFormat ($array_days, $arrayDatasets, $arrayOfArrayOptions);

	// display it
	displayChartStats ($chartName, $jsonStats);
}

// ################# PHP_EOL ###################
echo PHP_EOL;

// ################# FRAMASOFT ###################
$tmp_service 		= 'framastats';
$tmp_dateFirstStat	= '2015-07-26';

// *** Daily visits
$tmp_statName1	 	= 'rest_json_nbVisits_today_total'; 
$tmp_chartName		= 'framasoft_chartToday'; 
$tmp_arrayStatsName	= array($tmp_statName1);

handleChartsDatas_DaysAndValues ($dBase, $tmp_service, $tmp_chartName, $tmp_arrayStatsName, $tmp_dateFirstStat);

// *** Total visits
$tmp_statName1	 	= 'rest_json_nbVisits_all_total';
$tmp_chartName		= 'framasoft_chartAll';
$tmp_arrayStatsName	= array($tmp_statName1);

handleChartsDatas_DaysAndValues ($dBase, $tmp_service, $tmp_chartName, $tmp_arrayStatsName, $tmp_dateFirstStat);

// ################# FRAMADATE ###################
$tmp_service 		= 'framadate';
$tmp_dateFirstStat	= '2015-07-24';

// *** Daily pulls created
$tmp_statName1	 	= 'mySQL_nbPulls_today_created';
$tmp_chartName		= 'framadate_chartToday';
$tmp_arrayStatsName	= array($tmp_statName1);

handleChartsDatas_DaysAndValues ($dBase, $tmp_service, $tmp_chartName, $tmp_arrayStatsName, $tmp_dateFirstStat);

// *** Total pulls created
$tmp_statName1	 	= 'mySQL_nbPulls_all_living';
$tmp_chartName		= 'framadate_chartAll';
$tmp_arrayStatsName	= array($tmp_statName1);

handleChartsDatas_DaysAndValues ($dBase, $tmp_service, $tmp_chartName, $tmp_arrayStatsName, $tmp_dateFirstStat);

// ################ FRAMASPHERE ##################
$tmp_service 		= 'framasphere';
$tmp_dateFirstStat	= '2015-07-24';

// *** Total users and activity
$tmp_chartName		= 'framasphere_users';
$tmp_statName1		= 'total_users';
$tmp_statName2		= 'active_users_halfyear';
$tmp_statName3		= 'active_users_monthly';
$tmp_arrayStatsName	= array($tmp_statName1, $tmp_statName2, $tmp_statName3);
$arrayOfArrayOptions	= array(array ('label' => 'Total users'),array ('label' => 'Active Users Halfyear'),array ('label' => 'Active Users Monthly'));

handleChartsDatas_DaysAndValues ($dBase, $tmp_service, $tmp_chartName, $tmp_arrayStatsName, $tmp_dateFirstStat, $arrayOfArrayOptions);

// *** Posts
$tmp_chartName		= 'framasphere_posts';
$tmp_statName1		= 'local_posts';
$tmp_arrayStatsName	= array($tmp_statName1);

handleChartsDatas_DaysAndValues ($dBase, $tmp_service, $tmp_chartName, $tmp_arrayStatsName, $tmp_dateFirstStat);

// *** Comments
$tmp_chartName		= 'framasphere_comments';
$tmp_statName1		= 'local_comments';
$tmp_arrayStatsName	= array($tmp_statName1);

handleChartsDatas_DaysAndValues ($dBase, $tmp_service, $tmp_chartName, $tmp_arrayStatsName, $tmp_dateFirstStat);

// ################# FRAMABOOK ###################
$tmp_service 		= 'framabook';
$tmp_dateFirstStat	= '2015-07-24';

// *** Daily pulls created
$tmp_statName1	 	= 'mySQL_nbDwnlds_today';
$tmp_chartName		= 'framabook_chartToday';
$tmp_arrayStatsName	= array($tmp_statName1);

handleChartsDatas_DaysAndValues ($dBase, $tmp_service, $tmp_chartName, $tmp_arrayStatsName, $tmp_dateFirstStat);

// ################# FRAMADVD ###################
$tmp_service 		= 'framadvd';
$tmp_dateFirstStat	= '2015-07-24';

// *** Daily pulls created
$tmp_statName1	 	= 'logs_downloads_all';
$tmp_chartName		= 'framadvd_chartAll';
$tmp_arrayStatsName	= array($tmp_statName1);

handleChartsDatas_DaysAndValues ($dBase, $tmp_service, $tmp_chartName, $tmp_arrayStatsName, $tmp_dateFirstStat);

// ################# FRAMABIN ###################
$tmp_service 		= 'framabin';
$tmp_dateFirstStat	= '2015-07-24';

// *** Multiple datasets on the same graph
$tmp_chartName		= 'framabin_chartDocs';
$tmp_statName1		= 'logs_sharedDocs_all';
$tmp_statName2		= 'logs_docCreated_all';
$tmp_arrayStatsName	= array($tmp_statName1, $tmp_statName2);
$arrayOfArrayOptions	= array(array ('label' => 'Consulted documents'),array ('label' => 'Created documents'));

handleChartsDatas_DaysAndValues ($dBase, $tmp_service, $tmp_chartName, $tmp_arrayStatsName, $tmp_dateFirstStat, $arrayOfArrayOptions);

// ############## SOCIALS NETWORKS ################
$tmp_service 		= 'reseauxSociaux';
$tmp_dateFirstStat	= '2015-07-24';

// *** Twitter Followers
$tmp_chartName		= 'twitter_followers';
$tmp_statName1		= 'twitter_nbFollowers';
$tmp_arrayStatsName	= array($tmp_statName1);

handleChartsDatas_DaysAndValues ($dBase, $tmp_service, $tmp_chartName, $tmp_arrayStatsName, $tmp_dateFirstStat);

// *** Twitter Tweets
$tmp_chartName		= 'twitter_tweets';
$tmp_statName1		= 'twitter_nbTweets';
$tmp_arrayStatsName	= array($tmp_statName1);

handleChartsDatas_DaysAndValues ($dBase, $tmp_service, $tmp_chartName, $tmp_arrayStatsName, $tmp_dateFirstStat);

// *** Facebook likes
$tmp_chartName		= 'facebook_likes';
$tmp_statName1		= 'facebook_nbLikes';
$tmp_arrayStatsName	= array($tmp_statName1);

handleChartsDatas_DaysAndValues ($dBase, $tmp_service, $tmp_chartName, $tmp_arrayStatsName, $tmp_dateFirstStat);

/*
// ################# PATTERN ###################
$tmp_service 		= 'framaxxx';
$tmp_dateFirstStat	= '2015-07-24';

// *** One dataset in a graph
$tmp_chartName		= 'framaxxx_chart1'; // => canvas Id in html
$tmp_statName1	 	= 'statName'; // statName in dBase
$tmp_arrayStatsName	= array($tmp_statName1);

handleChartsDatas_DaysAndValues ($dBase, $tmp_service, $tmp_chartName, $tmp_arrayStatsName, $tmp_dateFirstStat);

// *** Multiple datasets on the same graph
$tmp_chartName		= 'framaxxx_chart2';
$tmp_statName1		= 'statName1';
$tmp_statName2		= 'statName2';
$tmp_statName3		= 'statName3';
$tmp_arrayStatsName	= array($tmp_statName1, $tmp_statName2, $tmp_statName3);
$arrayOfArrayOptions	= array(array ('label' => 'Plip plip'),array ('otherOptions' => 'Plop plop'),array ('label' => 'Plap'));

handleChartsDatas_DaysAndValues ($dBase, $tmp_service, $tmp_chartName, $tmp_arrayStatsName, $tmp_dateFirstStat, $arrayOfArrayOptions);
*/

?>
