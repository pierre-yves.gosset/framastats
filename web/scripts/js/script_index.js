//When DOM loaded
$(document).ready(function() {

	// ############ Fill div with stats (that are in div getStats ############
	$( "#getStats span" ).each(function(){

		// Retrieve each stat
		var nameStat = $(this).attr('class');
		var valueStat = $(this).html();

		// Fill div title with the same class name
		var selector = ".statToFill." + nameStat;
		$(selector).html(valueStat);
	});

	// ########### Change iconDisplay if panel is hidden or shown ############
	$( ".panel-collapse.collapse.withChevron" ).on('hidden.bs.collapse', function(){
		selectorIcon = "a[href='#" + $(this).attr('id') + "']"; // retrive panel that I've been collapse
		$(selectorIcon).find('.glyphicon').attr('class', 'glyphicon glyphicon-chevron-down');
		$(selectorIcon).parent()
				.mouseenter(function() {
				  $(this).css("background-color","#e0e0e0");
				})
				.mouseleave(function() {
				  $(this).css("background-color","inherit");
				});
	});
	$( ".panel-collapse.collapse.withChevron" ).on('shown.bs.collapse', function(){
		selectorIcon = "a[href='#" + $(this).attr('id') + "']";
		$(selectorIcon).find('.glyphicon').attr('class', 'glyphicon glyphicon-chevron-up');
		$(selectorIcon).parent()
				.mouseenter(function() {
				  $(this).css("background-color","#e0e0e0");
				})
				.mouseleave(function() {
				  $(this).css("background-color","inherit");
				});
	});
	
	$( ".panel-heading" ).click(function() {
		var selector = $(this).attr('link');
		$(selector).click();
	});

});
