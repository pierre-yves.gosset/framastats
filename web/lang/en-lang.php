<?php
/*
-----------------
Language: English
-----------------
*/

//---------------------------------------------------------
// index.php and other places
//---------------------------------------------------------

define('TXT_DESCRIPTION_SITE', "Public statistics tool for all <a href='http://www.framasoft.org'>Framasoft</a> services.");
define('TXT_SHOULD_NOT_APPEAR', "Error loading stat");
define('TXT_LABEL_CHEVRON_DOWN', "aria-label='Display more statistics'");

//---------------------------------------------------------
// Framadate
//---------------------------------------------------------

define('TXT_FRAMADATE_STAT_ALL_TITLE', "Pulls created since time immemorial");
	 
?> 
