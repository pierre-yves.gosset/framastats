# Framastats

## Description
La finalité du projet est d'être un site web permettant de visualiser des statistiques portant sur l'ensemble des services de Framasoft.    
Le site est visible en ligne à cette adresse : [framastats.org](http://framastats.org/)

## Présentation globale
Le projet se découpe en plusieurs modules/(scripts/parties/bidules) principaux.
   * Un contrôleur principal de constuction des statistiques pour chaque service (service = framadate/framakey/framadate...) : **main.php**
   * Un contrôleur permettant d'enregistrer toutes ces statistiques dans une base de données : **admin/recordAllStats.php**
   * Un contrôleur récupérant et traitant les statistiques dans la base de données pour les fournir à la Vue : **web/scripts/getStats.php**
   * Une vue gérant l'affichage de toutes ces statistiques : dossier **web/**
   * Un contrôleur déployant le dossier web du dossier git au dossier final : **admin/deployWeb.php**

## Installation - création des statistiques pour un nouveau service : main.php
1a) Se placer en ligne de commande dans le dossier du service, dans un endroit non accessible publiquement. (ex : /var/www/nouveauService/private/)   
1b) Créer un dossier qui contiendra tout le dépôt et aussi les fichiers sensibles crées (clés API etc.)  
```bash
    mkdir nameOfYourFramastatsFolder
```
2) Se rendre dans votre dossier et importer les fichiers du dépôt en clonant le dépot
```bash
    cd nameOfYourFramastatsFolder/
    git clone https://git.framasoft.org/quentin.dupont/framastats.git
```
3) Se rendre dans `framastats/` et lancer la commande suivante pour initialiser le nouveau service 
```bash
    cd framastats/
    php main.php nomDuNouveauService init
```
4) Répondre au script pour remplir les informations nécessaires
   * l'url du service : framaNouveau.org/ (paramètre informel) 
   * le chemin vers le dossier web : relative/path/to/the/web/folder
   * le type de base de données utilisées : doit être une bdd présente dans la liste donnée (aller modifier le fichier si plusieurs bdd)   

5) Pour certains modules nécessitant clés, mot de passe... lancer la commande suivante
```bash
    php main.php nomDuNouveauService
```    
Et répondre au script pour lui donner les informations dont le module a besoin.   

6) Ecrire votre code à l'endroit adéquate. Voir [le rôle des fichiers](https://git.framasoft.org/quentin.dupont/framastats/tree/master#le-r-le-des-fichiers).   

## Appel du script
```bash
   php main.php nomDuService (debug)
```
* nomDuService : Framadvd | Framalibre ...   
* debug : non obligatoire -> activera ou non les lignes de debug   

## Le rôle des fichiers
  * __framastats/__ : Dossier git du projet
     * __main.php__
       * Crée une instance de la classe Utils, c'est ici qu'on choisit le mode debug ou pas.
       * Vérifie l'appel du script
       * Fais compléter les informations manquantes à l'utilisateur si besoin
       * Établit les informations nécessaires pour la suite
       * Appelle scripts/dispatcher.php
     * __scripts/__ : Comporte tous les scripts écrits pour ce projet rangés par "base de données". 
       * __dispatcher.php__
         * Appel le ou les scripts correspondant au(x) service(s) demandé(s)
         * Récupère les stats ainsi créees (classe Stats)
         * Déploie les stats en ligne
             * Transforme les stats d'une classe Stats en format JSON
             * Place les stats dans un dossier tmpStats/ placé au même niveau que le dossier git framastats/
             * Copie ces stats en ligne grâce au chemin indiqué dans pathWebForServices.json
       * __mySQL/__ (Explication valable pour les autres dossiers)
         * __mainScript.php__
             * Établit la connexion à la base de données du service avec les informations enregistrées dans dbInfos.json (au niveau du dossier git)
             * Appelle le fichier de requêtes selon le service
         * __req_framaService.php__
             * Requête précisément la base de donnée du ce service et crée une classe Stats
       * __logsApache/__
       * __piwick/__
       * __socialsNetworks/__ : twitter et facebook
       * __utils/__ avec Util.php comportant un ensemble de fonctions génériques
     * __config/__ : contient des informations publiques expliquées [dans ce paragraphe](https://git.framasoft.org/quentin.dupont/framastats/tree/master#fichiers-de-configuration)
     * __vendor/__ : dossier lié au gestionnaire de paquets Composer
     * __install/__ : getRepoFramastats.php permet d'installer le dossier git rapidement (temporaire)

## Fichiers de configuration
Les informations demandées à l'utilisateur à la première utilisation sont enregistrées puis conservées dans le dossier __config/__.  
Voici une description de chaque fichier potentiellement crée :  

  * __Informations publiques__ dans le git (framastats/config/)
    * urlsForServices.json : indique l'URL où l'on trouvera le fichier de stats (sert de pense-bête pendant le développement)
    * pathWebForService.json : indique le chemin relatif permettant de mettre en ligne le fichier statistics.json
    * databaseForService.json : indique quel(s) type(s) de données le script va devoir gérer. (piwik|mySQL...)
  * __Informations privées__ (enregistrées au niveau du dossier framastats)
    * dbInfos.json : contient les infos nécessaires pour se connecter à une base de données de type mySQL
    * apiKeysConnection_facebook.json : contient les clés nécessaires à l'api rest de facebook
    * apiKeysConnection_twitter.json : contient les clés nécessaires à l'api rest de twitter
    * tokenAuth.json : contient la clé necessaire à l'api rest Piwick

      
## Mises à jour avec Cron
Cronjob rajouté à la /etc/crontab.   
Choix d'une mise à jour du script tous les jours à 22h45 et de lancer le script toutes les X minutes (ici 5 minutes).   

```bash
    45 22   * * *   root    cd /path/to/framastats/ && git pull >/dev/null 2>&1   
    */5 *   * * *   root    cd /path/to/framastats/ && php main.php nomDuFramaService
```

Penser à rajouter tout en haut ton e-mail pour recevoir les résultats du script.

```bash
   MAILTO=ton.adresse@mail.truc
```