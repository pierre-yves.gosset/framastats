<?php

/*
 * Record all statistics in database framastats_history
 * and all embedded service in framastats_service
 *
 * */

// ################# START TIME ###################
$timeStart	= microtime(true);

// ################## INCLUDE #####################
$pathToUtil_mySQL 	= '../scripts/mySQL/Util_mySQL.php';
$pathToUtil		= '../scripts/utils/Util.php';
include ($pathToUtil_mySQL);
include ($pathToUtil);

// ############### UTILS & DEBUG ##################
$util = new Util();
$util->setDebug("error");

// ################# CONSTANTS ####################
$nbOfArguments		= 1;
$nbOfArgumentsDebug	= $nbOfArguments + 1;
$error			= "### ERROR ###";
$man			= "You should call script as follows :" . PHP_EOL .
			  "> php recordAllStats.php (debug)";
$scriptEnds 		= "### SCRIPT ENDS ###" ;

$urlFramastats		= 'http://framastats.org/statistics.json';
$pathDbInfos		= '../../dbInfos_Framastats.json';
$pathStatsNotInDbase	= '../config/statsNotInDbase.json';
$pathStatsPossiblyNull	= '../config/statsPossiblyNull.json';
$config_keyForAll	= 'all';

$defaultInfo		= 'TO_CHANGE';
$tableFramastats_hstry	= 'framastats_history';
$tableFramastat_srvc	= 'framastats_service';

// #################### DATABASE ##################
$dbInfos = array (
	'db_host' 	=> $defaultInfo,
	'db_dbname' 	=> $defaultInfo,
	'db_usr' 	=> $defaultInfo,
	'db_pswrd' 	=> $defaultInfo
	);

$fields_framastats_history = array (
	'id_stat' => 'int NOT NULL AUTO_INCREMENT',
	'service' => 'varchar(24) NOT NULL',
	'nameStat' => 'varchar(128) NOT NULL',
	'valueStat' => 'varchar(64) NOT NULL',
	'date' => 'datetime NOT NULL'
	);

$fields_framastats_service = array (
	'id_service' => 'int NOT NULL AUTO_INCREMENT',
	'service' => 'varchar(24) NOT NULL',
	'url'=> 'varchar(64) NOT NULL'
	);

// ############## FUNCTIONS ###################

$tmp_result = array();
/*
 * Return a simple array with all the stats of the @array
 *
 * */
function recursiveDeepSearchStatInArray ($array) {
	global $tmp_result;

	// Init array to avoid older values
	$tmp_result = array();

	deepSearchStatInArray($array, $prefix = "");
	return $tmp_result;
}

/*
 * Called by recursiveDeepSearchStatInArray
 * */
function deepSearchStatInArray($array, $prefix = "") {
	global $tmp_result;

	if (is_array($array) && !empty($array)) {
		foreach ($array as $key => $value) {
			deepSearchStatInArray($value, "{$prefix}_{$key}");
		}
	} else {
		$tmp_result[substr($prefix, 1)] = (string) $array;
	}
}

function insertStatIntoDatabase_history ($dBase,$_tableName, $_service, $_nameStat, $_valueStat, $_date) {
	$req = $dBase->prepare("INSERT INTO $_tableName(service, nameStat, valueStat, date) VALUES(:service, :nameStat, :valueStat, :date)");
	$req->execute(array(
	    'service' => $_service,
	    'nameStat' => $_nameStat,
	    'valueStat' => $_valueStat,
	    'date' => $_date
	    ));
}

/*
 * insert url/service if it doesn't exists
 * */
function insertStatIntoDatabaseServiceOrNot ($dBase,$_tableName, $_service, $_url) {
	if (checkAnEntry($dBase, $_tableName, $_service) != 1) {
		$req = $dBase->prepare("INSERT INTO $_tableName(service, url) VALUES(:service, :url)");
		$req->execute(array(
		    'service' => $_service,
		    'url' => $_url
		    ));
	}

}

/*
 * check if the @service exists in @dBase
 * */
function checkAnEntry ($dBase, $tableName, $service) {
	$sql = "SELECT COUNT(*) FROM $tableName WHERE `service` LIKE '%$service%'";
	$res = $dBase->query($sql);
	return $res->fetch()['COUNT(*)'];
}

/*
 * Remove from @array all values that are forbidden : present in @pathJSONFile
 * return number of stats removed
 * */
function cleanStats ($_pathJSONFile, $_array, $_service) {
	global $util;
	$count = 0;

	$forbiddenStatsForAService = $util->getValuesWithKey_inJSONFile ($_pathJSONFile, $_service, false);

	// If file for the service exists
	if ($forbiddenStatsForAService != -1) {
		foreach ($forbiddenStatsForAService as $stat) {
			if (key_exists($stat, $_array)) {
				unset($_array[$stat]);
				$count += 1;
			}
		}
	}
	$util->out("Removed $count stats for $_service", 'info');
	return $_array;
}

/*
 * Check if the value seems correct and can be recorded
 * For the moment, just check if the @_value is not null and not in exception array, check below
 * @return -1 if not, 0 if it's correct
 * */
function checkValueStat ($_service, $_nameStat, $_valueStat) {
	global $util;
	global $pathStatsPossiblyNull;
	global $config_keyForAll;

	// Stat can't be null
	if ($_valueStat == null) return -1;

	// Cast to float
	$floatStat = (float) $_valueStat;

	// Check if null
	if ($floatStat == 0) {
		// Check if allowed to be null (for all services)
		$tmp_array = $util->getValuesWithKey_inJSONFile ($pathStatsPossiblyNull, $config_keyForAll, false);
		if ($tmp_array != -1) {
			if (in_array($_nameStat, $tmp_array)) {
				$found = true;
			} else {
				$found = false;
			}
		}
		// (for this service)
		if (!$found) {
			$tmp_array = $util->getValuesWithKey_inJSONFile ($pathStatsPossiblyNull, $_service, false);
			if ($tmp_array != -1) {
				if (in_array($_nameStat, $tmp_array)) {
					return 0;
				} else {
					return -1;
				}
			}
		}
	}
	else return 0;
}

// ############## MAIN SCRIPT ###################
$util->out("### Record All Stats", "info");

$mode = null;

// Check number of arguments
if ($argc < $nbOfArguments || $argc > $nbOfArgumentsDebug) {
	$util->out($error . PHP_EOL . $man . PHP_EOL . $scriptEnds, "error", true); // exit
}
// Debug And Init Mode
elseif ($argc == $nbOfArgumentsDebug) {
	$mode = $argv[$nbOfArgumentsDebug-1];
	if ($mode == "debug") {
		$util->setDebug("all");
		$modeUpperCase =  mb_strtoupper($mode);
		$util->out("### $modeUpperCase MODE", "info");
	} else {
		$util->out($error . PHP_EOL . $man . PHP_EOL . $scriptEnds, "error", true); // exit
	}
}

// Manage database informations
$finalDbInfos = $util->checkVariablesOrSetThem($pathDbInfos, $dbInfos, $defaultInfo);

// Database connection
try
{
	$dBase = new PDO('mysql:host=' . $finalDbInfos['db_host'] . ';dbname='. $finalDbInfos['db_dbname'] . ';charset=utf8', $finalDbInfos['db_usr'], $finalDbInfos['db_pswrd']);
	$util->out("### Established connection in the database : " . $finalDbInfos['db_dbname'], "success");
}
catch(Exception $e)
{
	$util->out("### Error in database connection : ".$e->getMessage(), "error");
	$util->out("### Check infos in this file : ". $pathDbInfos, "error", true);
}

// Create DbTables
$sql = Util_mySQL::createDbtable($dBase, $tableFramastats_hstry, $fields_framastats_history);
if($sql == -1) {
	$util->out("### Error in creating $tableFramastats_hstry", 'error', true); // exit
}

$sql = Util_mySQL::createDbtable($dBase, $tableFramastat_srvc, $fields_framastats_service);
if($sql == -1) {
	$util->out("### Error in creating $tableFramastat_srvc", 'error', true); // exit
}

//Retrieving embeddedServices
$jsonFramastats 		= json_decode(file_get_contents($urlFramastats), true);
$arrayEmbeddedServices		= $jsonFramastats['rest_json']['embeddedServices'];
//$arrayEmbeddedServices		= array ("framastats" => "http://framastats.org/statistics.json");

// For each service create and array with stat
foreach ($arrayEmbeddedServices as $service => $url) {

	// Init chrono service
	$timeStartService	= microtime(true);
	$util->out("### Service : " . $service);

	// Insert inDbase service and url
	insertStatIntoDatabaseServiceOrNot ($dBase,$tableFramastat_srvc, $service, $url);

	// Creating stats ready to dBase...
	$arrayStats 		= json_decode(file_get_contents($url), true);
	$arrayStats_modif	= recursiveDeepSearchStatInArray($arrayStats);

	// Retrieve file with forbidden stats (file named like service)
	$arrayStats_clean	= cleanStats($pathStatsNotInDbase, $arrayStats_modif, $service);

	// Check integrity of values, if one is not good, cancel recording for this service
	$validService = true;
	foreach ($arrayStats_clean as $nameStat => $valueStat) {
		if (checkValueStat ($service, $nameStat, $valueStat) == -1) {
			$invalidStat = $nameStat;
			$validService = false;
			break;
		}
	}
	if (!$validService) {
		$util->out("### Stats of $service are not recorded because the stat $invalidStat is not valid.", 'error');
		continue; // Don't record this service
	}

	// Insert in dBase for each stat
	$dateTime 		= date('Y-m-d H:i:s');
	foreach ($arrayStats_clean as $nameStat => $valueStat) {
		insertStatIntoDatabase_history ($dBase,$tableFramastats_hstry, $service, $nameStat, $valueStat, $dateTime);
	}

	// End chrono service
	$timeEndService		= microtime(true);
	$timeService		= $timeEndService - $timeStartService;
	$util->out("### $service took $timeService seconds to record");

}

// ################# END TIME ###################
$timeEnd 	= microtime(true);
$time		= $timeEnd - $timeStart;
$util->out("### Request took $time seconds", 'info');

?>
