<?php

/*
 * include by logsApache/mainScript.php
 * 
 * Create $stats
 * 
 * */

// ################## CONSTANTS ###################
$totalVisits_start		= 0;
$totalDocCreated_start		= 0;
$totalSharedDocs_start		= 0;
$statusCode 			= 200;
$minBytesDownload		= -1;
$referers	 		= array ("");

$regex_dayOfLastLog 		= "/[0-9]{2}.\/[a-zA-Z]+.\/[0-9]{4}/";

$regex_visit 			= "/^.*GET \/ /";
$regex_post 			= "/^.*POST \/ HTTP\/1.1\" 200 .+ \"https:\/\/framabin.org\/\"/";
$regex_sharedDoc		= "/^.*GET \/\?/";
$regex_name_visit		= 'visits';
$regex_name_post		= 'docCreated';
$regex_name_sharedDoc		= 'sharedDocs';
$regex_control_visit		= "/ \/ [A-Z]*\/([0-9]*).?([0-9]*)\" ([0-9]*) ([0-9]*) (\".*\") (\".*\")/";
$regex_control_sharedDod	= "/ \/\?[a-zA+Z0-9]+ [A-Z]*\/([0-9]*).?([0-9]*)\" ([0-9]*) ([0-9]*) (\".*\") (\".*\")/";
				// capturing group 3 ==> status code HTTP
				// capturing group 4 ==> bytes Downloaded
				// capturing group 5 ==> referer
				// capturing group 6 ==> client browser

// ################### SCRIPT #####################

$numberOfVisits = Util_logsApache::dealsWithStats ($util, $a_logs, $regex_visit, $regex_name_visit, $regex_control_visit, $statusCode, $minBytesDownload);
$numberOfPosts = Util_logsApache::dealsWithStats ($util, $a_logs, $regex_post, $regex_name_post);
$numberOfSharedDocs = Util_logsApache::dealsWithStats ($util, $a_logs, $regex_sharedDoc, $regex_name_sharedDoc, $regex_control_sharedDod, $statusCode, $minBytesDownload);

// #################### STATS #####################
// *** Retrieve day of logs ***
preg_match($regex_dayOfLastLog, json_encode($a_logs), $matches);
$logDate = $matches[0];

$stats = Util_logsApache::dealWithStats($stats, $tmp_pathFileName, $totalVisits_start, $numberOfVisits, $logDate);
$stats = Util_logsApache::dealWithStats($stats, $tmp_pathFileName, $totalDocCreated_start, $numberOfPosts, $logDate, 'docCreated');
$stats = Util_logsApache::dealWithStats($stats, $tmp_pathFileName, $totalSharedDocs_start, $numberOfSharedDocs, $logDate, 'sharedDocs');
?>
