<?php

class Util_mySQL
{
	/*
	 * @nametable : name of mySQL table
	 * @columnDate : column name of date creation
	 * @interval : interval we-re looking in
	 * @conditon : add a condition to the request "AND plop > ..."
	 * return : number of stats in the interval
	 * */
	public static function countSinceXDays($dBase, $nameTable, $columnDate, $interval, $condition = null)
	{
		$sql = "SELECT COUNT(*) FROM $nameTable WHERE DATE_SUB(CURDATE(),INTERVAL $interval) <= $columnDate ";
		if ($condition != null) {
			$sql .= $condition;
		}
		$res = $dBase->query($sql);
		$nb =  $res->fetch();
		return (int) $nb['COUNT(*)'];
	}

	/*
	 * @nametable : name of mySQL table
	 * @columnDate : column name of date creation
	 * @interval : interval we-re looking in
	 * return : sum of stats in the interval
	 * */
	public static function sumSinceXDays($dBase, $nameTable, $columnStat, $columnDate, $interval, $offset = null)
	{
		if ($interval == 'ALL') {
			$res = $dBase->query("SELECT SUM($columnStat) FROM $nameTable");
		} else {
			$sql = "SELECT SUM($columnStat) FROM $nameTable WHERE DATE_SUB(CURDATE(),INTERVAL $interval) <= $columnDate";
			$res = $dBase->query($sql);
		}

		$total =  $res->fetch()["SUM($columnStat)"];

		if ($offset != null) {
			$total += $offset;
		}

		return (int) $total;
	}

	/*
	 * Handle stats coming few times in the same day
	 * Save stat in @dBase for each day
	 *
	 * @stats : the famous instance of Stats we used all the time
	 * @dBase : handle by mainScript
	 * @tmp_pathFileName : path of tmp stats file
	 * @_totalStats_start : arbitrary number of visit
	 * @_stats_today : number of daily stats (if stats file didn't exist yet)
	 * @_nameTable of the dBase
	 * @_columnDate of the dBase
	 * @_nameStats : name to give to the stats (default : 'stats')
	 *
	 * return $stats
	 * */
	public static function dealWithStats ($stats, $dBase, $tableName, $tmp_pathFileName, $_totalStats_start, $_stats_today = 0, $_nameTable = null, $_columnDate = null, $_nameStats = 'stats', $_nameTable_new = null, $_columnDate_new = null, $_columnStat_new = null)
	{
		global $util;
		$stats_all_created		= $_nameStats . '_all_created';
		$stats_1lastYear_created	= $_nameStats . '_1lastYear_created';
		$stats_6lastMonths_created	= $_nameStats . '_6lastMonths_created';
		$stats_3lastMonths_created	= $_nameStats . '_3lastMonths_created';
		$stats_1lastMonth_created	= $_nameStats . '_1lastMonth_created';
		$stats_1lastMonth_removed	= $_nameStats . '_1lastMonth_removed';
		$stats_1lastWeek_created	= $_nameStats . '_1lastWeek_created';
		$stats_1lastWeek_removed	= $_nameStats . '_1lastWeek_removed';
		$stats_today_created		= $_nameStats . '_today_created';
		$stats_today_living		= $_nameStats . '_today_living';

		if (!file_exists($tmp_pathFileName)) // put all datas of the current day + an arbitrary number // It's first time we create stats file
		{
			$util->out("### Temporary stats file has never been created. ", 'info');
			$stats = Util_mySQL::createStatsVisit_init($stats, $_totalStats_start, $_stats_today, $_nameStats);

		}
		else // file exists and we hope is well_formed
		{
			$today = date("Y-m-d");

			// Retrieve old stats
			$arrayStatsLogs 	= json_decode(file_get_contents($tmp_pathFileName), true);
			$timeUpdateStats	= $arrayStatsLogs['mySQL']['timeUpdateStats'];
			$firstDayOfStats	= $arrayStatsLogs['mySQL']['firstDayOfStats'];
			$currentDayOfStats	= $arrayStatsLogs['mySQL']['currentDayOfStats'];

			$statsAll_old		= $arrayStatsLogs['mySQL'][$stats_all_created];
			$statsToday_old		= $arrayStatsLogs['mySQL'][$stats_today_created];

			// Find new ones
			$res 			= Util_mySQL::getStatsAfterSpecificDate($dBase, $_nameTable, $_columnDate, $timeUpdateStats);
			$statsToAdd 		= (int) $res['count'];
			$lastUpdateDateTime 	= $res['lastUpdateDateTime'];

			// Handle if we changed day or not
			$dateTimeLastUpdate 	= date_format(date_create($lastUpdateDateTime), "Y-m-d");

			// Handle recording in database
			if ($dateTimeLastUpdate != $today) { // New day : record all new pulls in the new day (even if some are from yesterday)
				$statsToday = $statsToAdd;
				Util_mySQL::insertIntoDatabase_Day_with_Stat($dBase,$tableName, $today, $statsToday);
			} else { // Same day : check if ther is an entry, as planned, or it was after init
				$statsToday = $statsToday_old + $statsToAdd;
				if (Util_mySQL::checkAnEntry($dBase, $tableName, $today) == 1) {
					Util_mySQL::updateIntoDatabase_Day_with_Stat($dBase,$tableName, $today, $statsToday);
				} else {
					Util_mySQL::insertIntoDatabase_Day_with_Stat($dBase,$tableName, $today, $statsToday);
				}
			}

			// Create stats
			$stats 						= Util_mySQL::createStatsVisit($stats, $statsToday, null, $_nameStats, $firstDayOfStats);

			$offset 					= 0; // TODO : adjust offset for each request when we will have a idea of a right stat
			// Retrieve thanks to dbase
			$stats->mySQL[$stats_all_created]		= Util_mySQL::sumSinceXDays($dBase, $_nameTable_new, $_columnStat_new, $_columnDate_new, 'ALL', $_totalStats_start);
			$stats->mySQL[$stats_1lastYear_created]		= Util_mySQL::sumSinceXDays($dBase, $_nameTable_new, $_columnStat_new, $_columnDate_new, '1 YEAR', $offset);
			$stats->mySQL[$stats_1lastYear_created]		= Util_mySQL::sumSinceXDays($dBase, $_nameTable_new, $_columnStat_new, $_columnDate_new, '1 YEAR', $offset);
			$stats->mySQL[$stats_6lastMonths_created]	= Util_mySQL::sumSinceXDays($dBase, $_nameTable_new, $_columnStat_new, $_columnDate_new, '6 MONTH', $offset);
			$stats->mySQL[$stats_3lastMonths_created]	= Util_mySQL::sumSinceXDays($dBase, $_nameTable_new, $_columnStat_new, $_columnDate_new, '3 MONTH', $offset);
			$stats->mySQL[$stats_1lastMonth_created]	= Util_mySQL::sumSinceXDays($dBase, $_nameTable_new, $_columnStat_new, $_columnDate_new, '1 MONTH', $offset);
			$stats->mySQL[$stats_1lastWeek_created]		= Util_mySQL::sumSinceXDays($dBase, $_nameTable_new, $_columnStat_new, $_columnDate_new, '1 WEEK', $offset);

		}

		return $stats;
	}

	/*
	 * Called by dealWithStats
	 * Look for stats which were created after @_dateTime
	 *
	 * @dateTime : should be like '2015-06-15 22:45:00'
	 * return array(dateTime of last entry, count of stats)
	 * */
	private static function getStatsAfterSpecificDate ($dBase, $nameTable, $columnDate, $_dateTime)
	{
		global $util;

		// Request
		$sql		= "SELECT $columnDate FROM $nameTable WHERE $columnDate > '$_dateTime' ORDER BY $columnDate DESC";
		$res 		= $dBase->query($sql);

		// Manage results
		$nb 		= $res->rowCount();
		if ($nb > 0) {
			$lastEntry = $res->fetch()[$columnDate];
		} else {
			$lastEntry = $_dateTime;
		}
		$util->out("### $nb pulls were created since $lastEntry");

		// Stats to give
		$res = array('lastUpdateDateTime' => $lastEntry, 'count' => $nb);
		return $res;
	}

	/*
	 * Called by dealWithStats
	 * */
	private static function createStatsVisit_init ($stats, $_totalStats_start, $_stats_today, $_nameStats = 'stats')
	{
		$stats_all_created			= $_nameStats . '_all_created';
		$stats_today_created			= $_nameStats . '_today_created';

		$stats->mySQL['firstDayOfStats']	= date("Y-m-d");
		$stats->mySQL['currentDayOfStats']	= date("Y-m-d");
		$stats->mySQL['timeUpdateStats'] 	= date("Y-m-d H:i:s");

		$stats->mySQL[$stats_today_created]	= $_stats_today;
		$stats->mySQL[$stats_all_created]	= $_totalStats_start + $stats->mySQL[$stats_today_created];
		return $stats;
	}

	/*
	 * Called by dealWithStats
	 * */
	private static function createStatsVisit ($stats, $_stats_today, $_stats_all = null, $_nameStats = 'stats', $_firstDayOfStats)
	{
		$stats_today_created			= $_nameStats . '_today_created';
		$stats_all_created			= $_nameStats . '_all_created';

		$stats->mySQL['firstDayOfStats'] 	= $_firstDayOfStats;
		$stats->mySQL['currentDayOfStats'] 	= date("Y-m-d");
		$stats->mySQL['timeUpdateStats'] 	= date("Y-m-d H:i:s");

		$stats->mySQL[$stats_today_created]	= $_stats_today;
		if ($_stats_all != null) {
			$stats->mySQL[$stats_all_created] = $_stats_all;
		}

		return $stats;
	}

	public static function createDbtable($dBase, $table, $fields)
	{
		$sql = "CREATE TABLE IF NOT EXISTS `$table` (";
	    	$pk  = '';

	    	foreach($fields as $field => $type)
	    	{
	      		$sql.= "`$field` $type,";

		     	if (preg_match('/AUTO_INCREMENT/i', $type)) {
				$pk = $field;
	      		}
	    	}

	    	$sql .= " PRIMARY KEY (`$pk`))";

	   	if($dBase->exec($sql) !== false) {
			 return $sql;
		} else return -1;
	}

	private static function insertIntoDatabase_Day_with_Stat ($dBase,$_tableName, $_day, $_stat) {
		$req = $dBase->prepare("INSERT INTO $_tableName(day, stat) VALUES(:day, :stat)");
		$req->execute(array(
		    'day' => $_day,
		    'stat' => $_stat
		    ));
	}

	private static function updateIntoDatabase_Day_with_Stat ($dBase,$_tableName, $day, $stat) {
		$req = $dBase->prepare("UPDATE $_tableName SET day = :day, stat = :stat WHERE day = :day");
		$req->execute(array(
		    'day' => $day,
		    'stat' => $stat
		    ));
	}

	/*
	 * Check if there is already en entry in the @_tableName in the @dBase
	 * corresponding to the @_day
	 * Update the @_stat or insert it
	 * */
	public static function handleInsertOrUpdate ($dBase,$_tableName, $_day, $_nameStat, $_stat) {

        global $util;
		$count = Util_mySQL::checkAnEntry($dBase, $_tableName, $_day);

		if ($count > 1) { // should not happen

			$util->out("### (Util_mySQL) Problem in handleInsertOrUpdate function, more than one stat a day", 'error');

			// Update all stat
			$sql_update = "UPDATE $_tableName SET $_nameStat = '$_stat' WHERE day LIKE '%$_day%'";
			$dBase->query($sql_update);

			// Delete the useless lines
			$howManyToDelete = $count - 1;
			$sql_delete = "DELETE FROM $_tableName WHERE day LIKE '%_day%' LIMIT $howManyToDelete";
			$dBase->query($sql_delete);

		} elseif ($count == 1) {
			$sql = "UPDATE $_tableName SET $_nameStat = '$_stat' WHERE day LIKE '%$_day%'";
			$req = $dBase->query($sql);
		} else {
			$req = $dBase->query("INSERT INTO $_tableName(day, $_nameStat) VALUES('$_day', $_stat)");
		}
	}

	private static function checkAnEntry ($dBase, $tableName, $day) {
		$sql = "SELECT COUNT(*) FROM $tableName WHERE `day` LIKE '%$day%'";
		$res = $dBase->query($sql);
		return $res->fetch()['COUNT(*)'];
	}

}
?>