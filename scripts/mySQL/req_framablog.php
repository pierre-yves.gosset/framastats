<?php

/*
 * include by mySQL/mainScript.php
 *
 * Create $stats
 *
 * */

// ################### CONSTANTS ##################
$nameTable_Options			= 'wp_fb_options';
$nameTable_Posts			= 'wp_fb_posts';
$nameTable_Comments			= 'wp_fb_comments';
$nameTable_Terms			= 'wp_fb_terms';
$nameTable_TermTaxonomy			= 'wp_fb_term_taxonomy';

$columnDate_comments			= 'comment_date';
$columnDate_posts			= 'post_date';

// ################### REQUESTS ##################
// *** Blogname
$res = $dBase->query("SELECT option_value FROM `$nameTable_Options` WHERE `option_name` = 'blogname'");
$blogName = $res->fetch();
$res->closeCursor();

// *** Nb of posts
$res = $dBase->query("SELECT COUNT(*) FROM `$nameTable_Posts` WHERE `post_status` = 'publish' AND `post_type` = 'post'");
$nbPosts_all = $res->fetch();
$res->closeCursor();

// *** Nb of category
$res = $dBase->query("SELECT COUNT(*) FROM `$nameTable_TermTaxonomy` WHERE `taxonomy` = 'post_tag'");
$nbCategory = $res->fetch();
$res->closeCursor();

// *** Top categories
$res = $dBase->query("SELECT ter.name, tax.count
			FROM `$nameTable_TermTaxonomy` tax
			INNER JOIN $nameTable_Terms ter ON tax.term_id = ter.term_id
			WHERE `taxonomy` = 'post_tag'
			ORDER BY tax.count desc
			LIMIT 10");
while ($data = $res->fetch())
{
	$tmpArray = array($data['name'] => $data['count']);
	$topCategory[] = $tmpArray;
}
$res->closeCursor();

// *** Nb of comments
$res = $dBase->query("SELECT COUNT(*) FROM `$nameTable_Comments` WHERE `comment_approved` = '1'");
$nbComments_all = $res->fetch();
$res->closeCursor();

// ##################### STATS ####################
$stats->mySQL['blogName']		= $blogName['option_value'];
$stats->mySQL['timeUpdateStats']	= date('Y-m-d H:i:s');

// *** Nb of posts
$stats->mySQL['nbPosts_all'] 		= (int) $nbPosts_all['COUNT(*)'];
$stats->mySQL['nbPosts_1lastYear']	= Util_mySQL::countSinceXDays($dBase, $nameTable_Posts, $columnDate_posts, '1 YEAR', "AND `post_status` = 'publish' AND `post_type` = 'post'");
$stats->mySQL['nbPosts_6lastMonths']	= Util_mySQL::countSinceXDays($dBase, $nameTable_Posts, $columnDate_posts, '6 MONTH', "AND `post_status` = 'publish' AND `post_type` = 'post'");
$stats->mySQL['nbPosts_3lastMonths']	= Util_mySQL::countSinceXDays($dBase, $nameTable_Posts, $columnDate_posts, '3 MONTH', "AND `post_status` = 'publish' AND `post_type` = 'post'");
$stats->mySQL['nbPosts_1lastMonth']	= Util_mySQL::countSinceXDays($dBase, $nameTable_Posts, $columnDate_posts, '1 MONTH', "AND `post_status` = 'publish' AND `post_type` = 'post'");
$stats->mySQL['nbPosts_1lastWeek']	= Util_mySQL::countSinceXDays($dBase, $nameTable_Posts, $columnDate_posts, '1 WEEK', "AND `post_status` = 'publish' AND `post_type` = 'post'");
$stats->mySQL['nbPosts_today']		= Util_mySQL::countSinceXDays($dBase, $nameTable_Posts, $columnDate_posts, '0 DAY', "AND `post_status` = 'publish' AND `post_type` = 'post'");

// *** Nb of category
$stats->mySQL['nbCategory'] 		= (int) $nbCategory['COUNT(*)'];

// *** Top category
$stats->mySQL['topCategory'] 		= $topCategory;

// *** Nb of comments
$stats->mySQL['nbComments_all'] 	= (int) $nbComments_all['COUNT(*)'];
$stats->mySQL['nbComments_1lastYear']	= Util_mySQL::countSinceXDays($dBase, $nameTable_Comments, $columnDate_comments, '1 YEAR', "AND `comment_approved` = '1'");
$stats->mySQL['nbComments_6lastMonths']	= Util_mySQL::countSinceXDays($dBase, $nameTable_Comments, $columnDate_comments, '6 MONTH', "AND `comment_approved` = '1'");
$stats->mySQL['nbComments_3lastMonths']	= Util_mySQL::countSinceXDays($dBase, $nameTable_Comments, $columnDate_comments, '3 MONTH', "AND `comment_approved` = '1'");
$stats->mySQL['nbComments_1lastMonth']	= Util_mySQL::countSinceXDays($dBase, $nameTable_Comments, $columnDate_comments, '1 MONTH', "AND `comment_approved` = '1'");
$stats->mySQL['nbComments_1lastWeek']	= Util_mySQL::countSinceXDays($dBase, $nameTable_Comments, $columnDate_comments, '1 WEEK', "AND `comment_approved` = '1'");
$stats->mySQL['nbComments_today']	= Util_mySQL::countSinceXDays($dBase, $nameTable_Comments, $columnDate_comments, '0 DAY', "AND `comment_approved` = '1'");

?>
