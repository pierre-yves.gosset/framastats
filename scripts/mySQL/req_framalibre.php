<?php

/*
 * include by mySQL/mainScript.php
 *
 * Create $stats
 *
 * */

// ################### CONSTANTS ##################
// The main section and his direct subsection (SELECT id_rubrique,id_parent FROM spip_rubriques WHERE id_parent= XX)
$allowedIdSubSections_LL = array("2","410","411","413","414","415","416","417","418","419","420","422","433","434","474");
$allowedIdSubSections_Articles = array("5","68","248","249","250","251","252","253","254","255","256","281","330","370","385","403","404","431");
$allowedIdSubSections_Tutos = array("4","261","262","263","264","265","266","267","268","269","270","399");

// ############### SPECIFIC FUNCTIONS #############
/*
 * Quick and dirty search
 * Count the number of notices with the tag "Logiciels libres" (id_mot = 140) and in section "Français" (id_secteur=338)
 * So somes notices are not in the real good section or subsection("Logiciels libres")
 * return count of notices that fits the request
 * */
function quickAndDirtySearchToCount ($dBase)
{
	global $util;

	$sql_countLogicielsLibres = "	SELECT COUNT(*)
					FROM spip_articles a
					INNER JOIN spip_mots_articles m ON a.id_article = m.id_article
					INNER JOIN spip_rubriques r ON a.id_rubrique=r.id_rubrique
					WHERE m.id_mot=140
					AND a.statut='publie'
					AND r.id_secteur=338";
	$countLogicielsLibres = $dBase->query($sql_countLogicielsLibres)->fetchColumn();
	$util->out("### Quick & Dirty final count : " . $countLogicielsLibres, "info");
	return $countLogicielsLibres;
}

/*
 * Recursive search. Start with section of the article and rise until it finds the right parent (or not)
 * Count the number of notices with the tag "Logiciels libres" (id_mot = 140) and in section "Français" (id_secteur=338) 
 * And which are part of the $allowedIdSubSections_LL
 * return tmp_stats with for the moment : count of notices that fits the request and the search
 * */
function recursiveSearchInSubsections_countNotices ($dBase, $intervalTime)
{
	global $util;

	// =================================
	$tmp_stats = array (
	    's_nbNotices' => '');

	global $allowedIdSubSections_LL;
	$countLogicielsLibres = 0;
	$req_intervalTime = "AND DATE_SUB(CURDATE(),INTERVAL $intervalTime) <= a.date";
	if ($intervalTime == -1)
	{
		$req_intervalTime = "";
	}
	// *** Notices with the tag "Logiciels libres" (id_mot = 140) and in section "Français" (id_secteur=338)
	$sql_logicielsLibres = 	"	SELECT r.id_rubrique, a.date, a.titre
					FROM spip_articles a
					INNER JOIN spip_mots_articles m ON a.id_article = m.id_article
					INNER JOIN spip_rubriques r ON a.id_rubrique=r.id_rubrique
					WHERE m.id_mot=140
					AND a.statut='publie'
					AND r.id_secteur=338
					$req_intervalTime";

	// =================================

	$res_logicielsLibres = $dBase->query($sql_logicielsLibres);
	$i = 0;
	// Count if the notice is in the right section (see $allowedIdSubSections_LL), otherwise copy it in $arrayToCheck
	while ($data = $res_logicielsLibres->fetch())
	{
		$section = $data['id_rubrique'];
		if (in_array($section, $allowedIdSubSections_LL))
		{
			$countLogicielsLibres += 1;
		}
		else
		{
			$arrayToCheck[$i] = $section;
		}
		$i += 1;
	}

	// Check in $arrayToCheck if the notice is in a subsection of the right section
	foreach ($arrayToCheck as $key => $idSection)
	{
			$sql_getIdParent = "SELECT id_parent FROM spip_rubriques WHERE id_rubrique=$idSection";
			$res = $dBase->query($sql_getIdParent);
			$data = $res->fetch();
			$idParent = $data['id_parent'];

			//echo "### Check for the section : " . $idSection .  " with the parent " . $idParent . PHP_EOL;

			if (in_array($idParent, $allowedIdSubSections_LL)) // Kind of articles we're looking for
			{
				//echo "### Right section : +1" .PHP_EOL;
				$countLogicielsLibres += 1;
				continue;
			}
			else if ($idParent == 0 ) // We arrived to the main section, it's not the kind of article we're looking for, we erase
			{
				//echo "### Not the right section." . PHP_EOL;
				//unset ($arrayIdRubriques[$key]);
				continue;
			}
			else
			{
				while ($idParent != 0 && !in_array($idParent, $allowedIdSubSections_LL))
				{
					//echo "### We go recursive" . PHP_EOL;
					$sql_getIdParent = "SELECT id_parent FROM spip_rubriques WHERE id_rubrique=$idParent";
					$res = $dBase->query($sql_getIdParent);
					//var_dump($res);
					$data = $res->fetch();
					$idParent = $data['id_parent'];
				}
				if ($idParent == 0)
				{
					//echo "### Not the right section." . PHP_EOL;
				}
				else
				{
					//echo "### Right section : +1" .PHP_EOL;
					$countLogicielsLibres += 1;
				}
			}
		}

		//echo "### Recursive final count - LL : " . $countLogicielsLibres . PHP_EOL;

		$tmp_stats['s_nbNotices'] = $countLogicielsLibres;
		return $tmp_stats;
}

/*
 * Recursive search. Start with section of the article and rise until it finds the right parent (or not)
 * Return the number of articles
 * */
function recursiveSearchInSubsections($dBase, $allowedIdSubSections, $intervalTime)
{
	global $util;

	// =================================
	$count = 0;
	$req_intervalTime = "AND DATE_SUB(CURDATE(),INTERVAL $intervalTime) <= a.date";
	if ($intervalTime == -1)
	{
		$req_intervalTime = "";
	}
	//  Should be in the section 338 (the french one)
	$sql = 	"SELECT titre, date, id_rubrique, id_secteur
		FROM spip_articles a
		WHERE statut='publie'
		AND id_secteur=338
		$req_intervalTime";
	// =================================

	$res = $dBase->query($sql);
	$i = 0;
	// Count if the notice is in the right section otherwise copy it in $arrayToCheck
	while ($data = $res->fetch())
	{
		$section = $data['id_rubrique'];
		if (in_array($section, $allowedIdSubSections))
		{
			$count += 1;
		}
		else
		{
			$arrayToCheck[$i] = $section;
		}
		$i += 1;
	}
	// Check in $arrayToCheck if the notice is in a subsection of the right section
	foreach ($arrayToCheck as $key => $idSection)
	{
			$sql_getIdParent = "SELECT id_parent FROM spip_rubriques WHERE id_rubrique=$idSection";
			$res = $dBase->query($sql_getIdParent);
			$data = $res->fetch();
			$idParent = $data['id_parent'];

			//echo "### Check for the section : " . $idSection .  " with the parent " . $idParent . PHP_EOL;

			if (in_array($idParent, $allowedIdSubSections)) // Kind of articles we're looking for
			{
				//echo "### Right section : +1" .PHP_EOL;
				$count += 1;
				continue;
			}
			else if ($idParent == 0 ) // We arrived to the main section, it's not the kind of article we're looking for, we erase
			{
				//echo "### Not the right section." . PHP_EOL;
				//unset ($arrayIdRubriques[$key]);
				continue;
			}
			else
			{
				while ($idParent != 0 && !in_array($idParent, $allowedIdSubSections))
				{
					//echo "### We go recursive" . PHP_EOL;
					$sql_getIdParent = "SELECT id_parent FROM spip_rubriques WHERE id_rubrique=$idParent";
					$res = $dBase->query($sql_getIdParent);
					//var_dump($res);
					$data = $res->fetch();
					$idParent = $data['id_parent'];
				}
				if ($idParent == 0)
				{
					//echo "### Not the right section." . PHP_EOL;
				}
				else
				{
					//echo "### Right section : +1" .PHP_EOL;
					$count += 1;
				}
			}
		}

		//echo "### Recursive final count : " . $count . PHP_EOL;
		return $count;
}

// ################### REQUESTS ##################
// *** Website name
$res = $dBase->query('SELECT valeur FROM spip_meta WHERE nom="nom_site"');
$webSiteName =  $res->fetch();

// *** Number of contributors
$res = $dBase->query('SELECT COUNT(DISTINCT id_auteur) FROM spip_auteurs_articles');
$nbAuthors =  $res->fetch();

// *** Number of articles with several authors
$res = $dBase->prepare('SELECT id_article, COUNT(*) as cnt FROM spip_auteurs_articles GROUP BY id_article HAVING cnt>1');
$exe =  $res->execute();
$nbArticleWithSeveralAuthors =  $res->rowCount();

// *** Number of comments
$res = $dBase->query('SELECT COUNT(*) FROM spip_forum WHERE statut="publie"');
$nbComments_all =  $res->fetch();

$res = $dBase->query('SELECT COUNT(*) FROM spip_forum WHERE DATE_SUB(CURDATE(),INTERVAL 1 YEAR) <= date_heure AND statut="publie"');
$nbComments_1lastYear =  $res->fetch();

$res = $dBase->query('SELECT COUNT(*) FROM spip_forum WHERE DATE_SUB(CURDATE(),INTERVAL 6 MONTH) <= date_heure AND statut="publie"');
$nbComments_6lastMonths =  $res->fetch();

$res = $dBase->query('SELECT COUNT(*) FROM spip_forum WHERE DATE_SUB(CURDATE(),INTERVAL 3 MONTH) <= date_heure AND statut="publie"');
$nbComments_3lastMonths =  $res->fetch();

$res = $dBase->query('SELECT COUNT(*) FROM spip_forum WHERE DATE_SUB(CURDATE(),INTERVAL 1 MONTH) <= date_heure AND statut="publie"');
$nbComments_1lastMonth =  $res->fetch();

$res = $dBase->query('SELECT COUNT(*) FROM spip_forum WHERE DATE_SUB(CURDATE(),INTERVAL 1 WEEK) <= date_heure AND statut="publie"');
$nbComments_1lastWeek =  $res->fetch();

$res = $dBase->query('SELECT COUNT(*) FROM spip_forum WHERE DATE(date_heure)=CURDATE() AND statut="publie"');
$nbComments_today =  $res->fetch();

// *** Number of visits
$res = $dBase->query('SELECT SUM(visites) FROM spip_visites');
$nbVisits_all =  $res->fetch();

$res = $dBase->query('SELECT SUM(visites) FROM spip_visites WHERE DATE_SUB(CURDATE(),INTERVAL 1 YEAR) <= date');
$nbVisits_1lastYear =  $res->fetch();

$res = $dBase->query('SELECT SUM(visites) FROM spip_visites WHERE DATE_SUB(CURDATE(),INTERVAL 6 MONTH) <= date');
$nbVisits_6lastMonths =  $res->fetch();

$res = $dBase->query('SELECT SUM(visites) FROM spip_visites WHERE DATE_SUB(CURDATE(),INTERVAL 3 MONTH) <= date');
$nbVisits_3lastMonths =  $res->fetch();

$res = $dBase->query('SELECT SUM(visites) FROM spip_visites WHERE DATE_SUB(CURDATE(),INTERVAL 1 MONTH) <= date');
$nbVisits_1lastMonth =  $res->fetch();

$res = $dBase->query('SELECT SUM(visites) FROM spip_visites WHERE DATE_SUB(CURDATE(),INTERVAL 1 WEEK) <= date');
$nbVisits_1lastWeek =  $res->fetch();

$res = $dBase->query('SELECT visites FROM spip_visites WHERE date=CURDATE()');
$nbVisits_today =  $res->fetch();

// ################### STATS ##################
$stats->mySQL['site']				= $webSiteName['valeur'];

//$stats->mySQL['s_nbNotices'] = quickAndDirtySearchToCount($dBase);

$stats->mySQL['nbNotices_all']	 		= recursiveSearchInSubsections_countNotices($dBase, "-1")['s_nbNotices'];
$stats->mySQL['nbNotices_1lastYear'] 		= recursiveSearchInSubsections_countNotices($dBase, "1 YEAR")['s_nbNotices'];
$stats->mySQL['nbNotices_3lastMonths'] 		= recursiveSearchInSubsections_countNotices($dBase, "3 MONTH")['s_nbNotices'];
$stats->mySQL['nbNotices_6lastMonths']		= recursiveSearchInSubsections_countNotices($dBase, "6 MONTH")['s_nbNotices'];

$stats->mySQL['nbArticlesTribune_all']		= recursiveSearchInSubsections($dBase, $allowedIdSubSections_Articles, "-1");
$stats->mySQL['nbTutoriels_all']		= recursiveSearchInSubsections($dBase, $allowedIdSubSections_Tutos, "-1");

$stats->mySQL['nbAuthors']			= (int) $nbAuthors['COUNT(DISTINCT id_auteur)'];
$stats->mySQL['nbArticlesWithSeveralAuthors']	= $nbArticleWithSeveralAuthors;

$stats->mySQL['nbComments_all'] 		= (int) $nbComments_all['COUNT(*)'];
$stats->mySQL['nbComments_1lastYear']		= (int) $nbComments_1lastYear['COUNT(*)'];
$stats->mySQL['nbComments_6lastMonths']		= (int) $nbComments_6lastMonths['COUNT(*)'];
$stats->mySQL['nbComments_3lastMonths']		= (int) $nbComments_3lastMonths['COUNT(*)'];
$stats->mySQL['nbComments_1lastMonth']		= (int) $nbComments_1lastMonth['COUNT(*)'];
$stats->mySQL['nbComments_1lastWeek']		= (int) $nbComments_1lastWeek['COUNT(*)'];
$stats->mySQL['nbComments_today']		= (int) $nbComments_today['COUNT(*)'];

$stats->mySQL['nbVisits_all'] 			= (int) $nbVisits_all['SUM(visites)'];
$stats->mySQL['nbVisits_1lastYear']		= (int) $nbVisits_1lastYear['SUM(visites)'];
$stats->mySQL['nbVisits_6lastMonths']		= (int) $nbVisits_6lastMonths['SUM(visites)'];
$stats->mySQL['nbVisits_3lastMonths']		= (int) $nbVisits_3lastMonths['SUM(visites)'];
$stats->mySQL['nbVisits_1lastMonth']		= (int) $nbVisits_1lastMonth['SUM(visites)'];
$stats->mySQL['nbVisits_1lastWeek']		= (int) $nbVisits_1lastWeek['SUM(visites)'];
$stats->mySQL['nbVisits_today']			= (int) $nbVisits_today['visites'];

$stats->mySQL['timeUpdateStats'] 		= date('Y-m-d H:i:s');

?>
