<?php

/*
 * include by main.php
 *
 * Call script according to user choices
 * Retrieve $stats and deploy them online
 *
 * */

// #################### CLASS #####################
class Stats
{
	// All stats are set in each module
}

// ################## CONSTANTS ###################

$statsFile 		= 'statistics.json';
$statsFile_size 	= strlen($statsFile);

$tmp_dirName 		= '../tmpStats';
$tmp_fileName		= '/tmp_statistics.json';
$tmp_pathFileName	= $tmp_dirName . $tmp_fileName;

$pathDbInfos 		= '../dbInfos.json';
$pathTokenFile		= '../tokenAuth.json';
$defaultInfo 		= 'TO_CHANGE';

$pathScript_mySQL	= 'mySQL/mainScript.php';
$pathScript_logsApache	= 'logsApache/mainScript.php';
$pathScript_twitter	= 'socialNetworks/twitter/tw_mainScript.php';
$pathScript_facebook	= 'socialNetworks/facebook/fb_mainScript.php';
$pathScript_piwik 	= 'piwik/mainScript.php';
$pathScript_rest_json 	= 'rest_json/mainScript.php';

$u_service 		= $userChoices['service'];
$u_database		= $userChoices['database'];
$u_pathToWeb		= $userChoices['pathToWeb'];

// ############ SPECIFIC FUNCTIONS ################

/*
 * Create path (url) to put statistics file in the right place
 * @_pathToWeb : path for statistics file (user choice)
 * @_statsFile : name of statistics file
 * return path
 * */
function CreatePathForJSONStatistics ($_pathToWeb, $_statsFile)
{
	$lastCharacter = substr($_pathToWeb, -1);
	if ($lastCharacter == '/')
	{
		return $_pathToWeb . $_statsFile;
	}
	else 	return $_pathToWeb . '/'. $_statsFile;
}

/*
 * @_dirName : path for the folder
 * @_chmod : chmod for the folder
 * @_pathFile
 * @_content of the file
 * */
function CreateDirectory_and_PutContentInFile ($_dirName, $_chmod, $_pathFile, $_content)
{
	global $util;

	mkdir($_dirName, $_chmod);
	$res = (file_put_contents($_pathFile, $_content)) ? " was a success." : " failed !!";
	$color = ($res == " was a success." ) ? "success" : "error";
	$util->out("### Creation of temporary stats file " . $_pathFile . $res, $color);
}

/*
 * Transforme your array and publish it online thanks to your choices
 * @_statsArray : the array of stats
 * */
function arrayToDeploymentInJSON ($_statsArray)
{
	global $util;

	global $tmp_dirName;
	global $tmp_pathFileName;
	global $statsJSON;
	global $u_pathToWeb;
	global $u_service;
	global $statsFile;
	global $statsFile_size;
	global $path_urlService;

	// Encoding in JSON
	$statsJSON = json_encode($_statsArray);

	// Writing in a private temporary file
	if (file_exists($tmp_dirName))
	{
		if (is_dir($tmp_dirName))
		{
			// Folder were already there
			$res = (file_put_contents($tmp_pathFileName, $statsJSON)) ? " was a success !!" : " failed !!";
			$color = ($res == " was a success !!" ) ? "success" : "error";
			$util->out("### Creation of temporary stats file " . $tmp_pathFileName . $res, $color);
		} else
		{
			// A file was named like our folder
			rename($tmp_dirName, $tmp_dirName . "_FILE_RENAMED");
			CreateDirectory_and_PutContentInFile($tmp_dirName, 0700, $tmp_pathFileName, $statsJSON);
		}
	} else
	{
		CreateDirectory_and_PutContentInFile($tmp_dirName, 0700, $tmp_pathFileName, $statsJSON);
	}

	// Copy file in public website thanks to user choice
	$pathFileName = CreatePathForJSONStatistics($u_pathToWeb, $statsFile);
	/*if ($util->confirmationToWriteInThisFolder($pathFileName, $statsFile, $statsFile_size) == 'yes')
	{*/
		$util->out("### Try to copy file in this path : " . $pathFileName);
		$_url = $util->getValueWithKey_inJSONFile($path_urlService, $u_service);
		$resCopy = (copy($tmp_pathFileName, $pathFileName)) ? "was a success. Public adress should be : $_url$statsFile" : "failed !!" ;
	/*}
	else
	{
		$resCopy = "failed !!" ;
	}*/
	$color = ($resCopy == "failed !!" ) ? "error" : "success";
	$util->out("### Online deployment " . $resCopy, $color);

}

// ############## DISPATCHER ###################

$stats = new Stats();

foreach ($u_database as $database) {
	switch ($database) {
		case "mySQL":
			// Create $stats
			include($pathScript_mySQL);
			break;
		case "logsApache":
			// Create $stats
			include($pathScript_logsApache);
			break;
		case "socialNetworks":
			// Create $stats
			$stats->site = "Social Networks";
			$stats->timeUpdateStats = date('Y-m-d');
			include($pathScript_twitter);
			include($pathScript_facebook);
			break;
		case "piwik":
			// Create $stats
			include($pathScript_piwik);
			break;
		case "rest_json":
			// Create $stats
			include($pathScript_rest_json);
			break;
		default:
			$util->out("### The module '$database' is not implemented yet -> No stats for this module.", "error");
			break;
	}
}

// Deployment online with the $stats
arrayToDeploymentInJSON($stats);


?>
