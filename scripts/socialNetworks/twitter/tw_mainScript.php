<?php

/*
 * include by dispatcher.php
 * 
 * Handle connection, requests and creation of $stats
 * 
 * */


// ################## INCLUDE #####################
require 'vendor/autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;

// ################# CONSTANTS ####################
$apiKeys = array (
'consumerKey' 		=> $defaultInfo,
'consumerSecret' 	=> $defaultInfo,
'accessToken' 		=> $defaultInfo,
'accessTokenSecret' 	=> $defaultInfo
);

$pathApiKeys		= '../apiKeysConnection_twitter.json';
$screen_name		= 'framasoft';

// ################### SCRIPT #####################
$util->out("### Stats with API Twitter ", "info");

$finalApiInfos = $util->checkVariablesOrSetThem ($pathApiKeys, $apiKeys, $defaultInfo);

// Set API Informations
$consumerKey		= $finalApiInfos['consumerKey'];
$consumerSecret		= $finalApiInfos['consumerSecret'];
$accessToken 		= $finalApiInfos['accessToken'];
$accessTokenSecret 	= $finalApiInfos['accessTokenSecret'];

// Connection to the API
$connection 		= new TwitterOAuth($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret);

// Request
$infos	 		= $connection->get('users/show', array('screen_name' => $screen_name));
// print_r($infos);

// $stats is created in the dispatcher
$stats->twitter['nbFollowers'] 	= $infos['followers_count'];
$stats->twitter['nbTweets'] 	= $infos['statuses_count'];
$stats->twitter['nbFriends']	= $infos['friends_count'];
?>
