<?php

/*
 * include by dispatcher.php
 * 
 * Handle connection, requests and creation of $stats
 * 
 * */

// ############## INCLUDE ETC... ##################
define('FACEBOOK_SDK_V4_SRC_DIR', 'vendor/facebook/php-sdk-v4/src/Facebook/');
define('__GRAPH_FB__', 'https://graph.facebook.com/');
define('__PAGE_ID__', 'framasoft');

require 'vendor/facebook/php-sdk-v4/autoload.php';

use Facebook\FacebookSession;
use Facebook\FacebookRequest;

// ################# CONSTANTS ####################
$apiKeys = array (
'appID' 		=> $defaultInfo,
'appSecret'	 	=> $defaultInfo,
);

$pathApiKeys		= '../apiKeysConnection_facebook.json';

// ################### SCRIPT #####################
$util->out("### Stats with API Facebook ", "info");
$finalApiInfos = $util->checkVariablesOrSetThem ($pathApiKeys, $apiKeys, $defaultInfo);

// Set API Informations
$appID			= $finalApiInfos['appID'];
$appSecret		= $finalApiInfos['appSecret'];
$accessToken		= $appID . '|' . $appSecret;

// Connection to the API
FacebookSession::setDefaultApplication($appID, $appSecret);
$session = FacebookSession::newAppSession($appID, $appSecret);

// To validate the session: 
try { 
  $session->validate(); 
} catch (FacebookRequestException $ex) {
  // Session not valid, Graph API returned an exception with the reason. 
  $util->out("FacebookRequestException : " . $ex->getMessage(), "error"); 
} catch (\Exception $ex) { 
  // Graph API returned info, but it may mismatch the current app or have expired. 
  $util->out("Exception : ". $ex->getMessage(), "error"); 
}

// Request
$request = new FacebookRequest(
  $session,
  'GET',
  '/'.__PAGE_ID__.'?fields=likes'
);
$response 	= $request->execute();
$res_Likes 	= $response->getGraphObject();

// Stats creation
$stats->facebook['nbLikes']= $res_Likes->getProperty('likes');
?>
