<?php

class Util_piwik
{
	/*
	 * Add a lot of stats on visits in $stats
	 * @idSite : id of the site in Piwik
	 * @token_auth : token for Piwik API
	 * @url : url base to Piwik site
	 *
	 * */
	public static function addVisitsStats ($stats, $idSite, $token_auth, $url, $util)
	{
		$reqEnd 			 = "&format=JSON&token_auth=$token_auth";
		$reqVisits 			 = $url . "?module=API&method=VisitsSummary.get" . $reqEnd;

		$stats->pwk['nbVisits_all']		= Util_piwik::visitsSinceXDays($idSite, $reqVisits, "2013-01-01,today", $util);
		$stats->pwk['nbVisits_thisYear']	= Util_piwik::visitsDuringThisPeriod($idSite, $reqVisits, "year", "today", $util);
		$stats->pwk['nbVisits_last365days']	= Util_piwik::visitsSinceXDays($idSite, $reqVisits, "last365", $util);
		$stats->pwk['nbVisits_last180days']	= Util_piwik::visitsSinceXDays($idSite, $reqVisits, "last180", $util);
		$stats->pwk['nbVisits_last90days']	= Util_piwik::visitsSinceXDays($idSite, $reqVisits, "last90", $util);
		$stats->pwk['nbVisits_last30days']	= Util_piwik::visitsSinceXDays($idSite, $reqVisits, "last30", $util);
		$stats->pwk['nbVisits_last7days']	= Util_piwik::visitsSinceXDays($idSite, $reqVisits, "last7", $util);
		$stats->pwk['nbVisits_yesterday']	= Util_piwik::visitsDuringThisPeriod($idSite, $reqVisits, "day", "yesterday", $util);
		$stats->pwk['nbVisits_today']		= Util_piwik::visitsDuringThisPeriod($idSite, $reqVisits, "day", "today", $util);

	}

	/*
	 * Called by addVisitsStats()
	 * Call Piwik API for visits
	 *
	 * */
	private static function visitsDuringThisPeriod($_idSite, $_reqVisits, $_period, $_days, $util)
	{
		$req 		= $_reqVisits . "&idSite=$_idSite&period=$_period&date=$_days";
		$jsonRes	= @file_get_contents($req);
		$arrayRes 	= json_decode($jsonRes, true);
		if ($arrayRes != FALSE) {
			return $arrayRes['nb_visits'];
		} else {
			$util->out("Error in Piwik API request", "error", true);
			return null;
		}
	}

	/*
	 * Called by addVisitsStats()
	 * Call Piwik API for visits
	 *
	 * */
	private static function visitsSinceXDays($_idSite, $_reqVisits, $_days, $util)
	{
		$req 		= $_reqVisits . "&idSite=$_idSite&period=range&date=" . $_days;
		$jsonRes	= @file_get_contents($req);
		$arrayRes 	= json_decode($jsonRes, true);

		if ($arrayRes == FALSE) {
			$util->out("Error in Piwik API request : No response from API", "error", true);
			return null;
		} elseif (!array_key_exists('nb_visits',$arrayRes)) {
			$util->out("Error in Piwik API request : No nb_visits in there response", "error", true);
			return null;
		} else {
			return $arrayRes['nb_visits'];
		}
	}

}
?>
