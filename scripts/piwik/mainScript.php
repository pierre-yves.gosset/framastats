<?php

/*
 * include by dispatcher.php
 *
 * Handle with connection to Piwik API
 * Redirect request according to the service.
 * Retrieve $stats
 *
 * */

// ################## INCLUDE #####################
$pathToUtil_Piwik = "Util_piwik.php";
include ($pathToUtil_Piwik);

// ################# CONSTANTS ####################
$url 					= "http://stats.framasoft.org/";
$pathReqDegooglisonsInternet_piwik	= "req_degooglisonsInternet.php";
$pathReqFramabag_piwik			= "req_framabag.php";
$pathReqFramabee_piwik			= "req_framabee.php";
$pathReqFramabin_piwik			= "req_framabin.php";
$pathReqFramablog_piwik			= "req_framablog.php";
$pathReqFramabook_piwik			= "req_framabook.php";
$pathReqFramacalc_piwik			= "req_framacalc.php";
$pathReqFramacloud_piwik		= "req_framacloud.php";
$pathReqFramacolibri_piwik		= "req_framacolibri.php";
$pathReqFramadate_piwik			= "req_framadate.php";
$pathReqFramadvd_piwik			= "req_framadvd.php";
$pathReqFramagames_piwik		= "req_framagames.php";
$pathReqFramagit_piwik			= "req_framagit.php";
$pathReqFramakey_piwik			= "req_framakey.php";
$pathReqFramalab_piwik			= "req_framalab.php";
$pathReqFramalibre_piwik		= "req_framalibre.php";
$pathReqFramalink_piwik			= "req_framalink.php";
$pathReqFramanews_piwik			= "req_framanews.php";
$pathReqFramapack_piwik			= "req_framapack.php";
$pathReqFramapad_piwik			= "req_framapad.php";
$pathReqFramapic_piwik			= "req_framapic.php";
$pathReqFramastart_piwik		= "req_framastart.php";
$pathReqFramasphere_piwik		= "req_framasphere.php";
$pathReqFramatube_piwik			= "req_framatube.php";
$pathReqFramavectoriel_piwik		= "req_framavectoriel.php";
$pathReqFramazic_piwik			= "req_framazic.php";
$pathReqFramindmap_piwik		= "req_framindmap.php";

// ################# FUNCTIONS ####################
/*
 * Create or ask for creation and read token
 * @_pathTokenFile : path to the json file
 * */
function dealsWithTokenFile ($_pathTokenFile)
{
	global 	$util;
	global 	$defaultInfo;
		$tokenAuth = array ('token_auth' => $defaultInfo);

	// Deals with the creation of the tokenAuth
	$flagCreateFile = true;
	if (file_exists($_pathTokenFile) && is_dir($_pathTokenFile))
	{
		$util->out("### A folder was named like our tokenAuth", "info");
		rename($_pathTokenFile, $_pathTokenFile . "_DIR_RENAMED");
		$flagCreateFile = file_put_contents($_pathTokenFile, json_encode($tokenAuth));
	}
	elseif (!file_exists($_pathTokenFile))
	{
		$flagCreateFile = file_put_contents($_pathTokenFile, json_encode($tokenAuth));
	}

	if(!$flagCreateFile)
	{
		$util->out("### Problem in writing tokenAuth", "error");
	}

	// Check if tokenAuth is well formed
	if (!$tmptokenAuth = json_decode(file_get_contents($_pathTokenFile),true))
	{
		$util->out("### TokenAuth (" . $_pathTokenFile . ") is not well-formed. Check JSON format. ", "error", true); // exit
	}

	// Asking user to fill missing informations
	foreach ($tmptokenAuth as $token_key => $token_value)
	{
		if ($token_value == $defaultInfo || trim($token_value) === '')
		{
			$handle = fopen ("php://stdin","r");
			echo "### Set the value for " . $token_key . " : " . PHP_EOL;
			$line = fgets($handle);
			$tmptokenAuth[$token_key] = trim($line);
			echo "### You set : " . $token_key . " => '" . $tmptokenAuth[$token_key] . "'" . PHP_EOL ;
		}
	}

	// Write file with the recent values
	if (file_put_contents($_pathTokenFile, json_encode($tmptokenAuth)))
	{
		$util->out("### You can change the tokenAuth here : " . $_pathTokenFile, "info");
	}
	else
	{
		$util->out("### Error in writing tokenAuth", "error", true); // exit
	}

	// Retrieve tokenAuth
	if (!$res = json_decode(file_get_contents($_pathTokenFile),true))
	{
		$util->out("### Error in reading tokenAuth in " . $_pathTokenFile . ". Check JSON format. ", "error", true); // exit
	}

	return $res;
}

// ################ MAIN SCRIPT ###################
$util->out("### Stats with Piwik", "info");
$stats->pwk['timeUpdateStats'] 	= date('Y-m-d H:i:s');

$res 		= dealsWithTokenFile($pathTokenFile);
$token_auth 	= $res['token_auth']; // token used to authenticate our API request.

// add stats in $stats
switch ($u_service) {
	case "degooglisonsInternet":
		include($pathReqDegooglisonsInternet_piwik);
		break;
	case "framabag":
		include($pathReqFramabag_piwik);
		break;
	case "framabee":
		include($pathReqFramabee_piwik);
		break;
	case "framabin":
		include($pathReqFramabin_piwik);
		break;
	case "framablog":
		include($pathReqFramablog_piwik);
		break;
	case "framabook":
		include($pathReqFramabook_piwik);
		break;
	case "framacalc":
		include($pathReqFramacalc_piwik);
		break;
	case "framacloud":
		include($pathReqFramacloud_piwik);
		break;
	case "framacolibri":
		include($pathReqFramacolibri_piwik);
		break;
	case "framadate":
		include($pathReqFramadate_piwik);
		break;
	case "framadvd":
		include($pathReqFramadvd_piwik);
		break;
	case "framagames":
		include($pathReqFramagames_piwik);
		break;
	case "framagit":
		include($pathReqFramagit_piwik);
		break;
	case "framakey":
		include($pathReqFramakey_piwik);
		break;
	case "framalab":
		include($pathReqFramalab_piwik);
		break;
	case "framalibre":
		include($pathReqFramalibre_piwik);
		break;
	case "framalink":
		include($pathReqFramalink_piwik);
		break;
	case "framanews":
		include($pathReqFramanews_piwik);
		break;
	case "framapack":
		include($pathReqFramapack_piwik);
		break;
	case "framapad":
		include($pathReqFramapad_piwik);
		break;
	case "framapic":
		include($pathReqFramapic_piwik);
		break;
	case "framastart":
		include($pathReqFramastart_piwik);
		break;
	case "framasphere":
		include($pathReqFramasphere_piwik);
		break;
	case "framatube":
		include($pathReqFramatube_piwik);
		break;
	case "framavectoriel":
		include($pathReqFramavectoriel_piwik);
		break;
	case "framazic":
		include($pathReqFramazic_piwik);
		break;
	case "framindmap":
		include($pathReqFramindmap_piwik);
		break;
	default :
		$util->out("### This service is not initialized for piwik", "error", true); //exit
		break;
}

?>
